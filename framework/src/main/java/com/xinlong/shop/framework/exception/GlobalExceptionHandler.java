package com.xinlong.shop.framework.exception;

import com.xinlong.shop.framework.common.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @Author Sylow
 * @Description 全局异常处理器
 * @Date: Created in 22:37 2022/11/6
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 拦截业务异常
     * @param e
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    public R handleServiceException(ServiceException e){
        logger.error(e.getMessage(), e);
        return R.error(e.getMessage());
    }

    /**
     * 拦截未知异常
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public R handleRuntimeException(RuntimeException e){
        logger.error(e.getMessage(), e);
        return R.error("系统发生异常" + e.getMessage());
    }
}
