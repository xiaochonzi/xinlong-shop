package com.xinlong.shop.framework.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 页面统计
 * </p>
 *
 * @author Sylow
 * @since 2023-05-10
 */
@TableName("sys_page_statistics")
public class SysPageStatistics implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 页面id
     */
    private String pageId;

    /**
     * 活动id
     */
    private Integer rushPurchaseId;

    /**
     * 访问时间
     */
    private Long visitTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public Integer getRushPurchaseId() {
        return rushPurchaseId;
    }

    public void setRushPurchaseId(Integer rushPurchaseId) {
        this.rushPurchaseId = rushPurchaseId;
    }

    public Long getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Long visitTime) {
        this.visitTime = visitTime;
    }

    @Override
    public String toString() {
        return "SysPageStatistics{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", pageId=" + pageId +
            ", rushPurchaseId=" + rushPurchaseId +
            ", visitTime=" + visitTime +
        "}";
    }
}
