package com.xinlong.shop.framework.core.model;

import com.xinlong.shop.framework.core.entity.SysRole;
import com.xinlong.shop.framework.core.entity.SysAdmin;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 03:35 2022/5/22
 */
public class LoginAdmin implements UserDetails {

    private SysAdmin sysAdmin;

    private List<SysRole> authoritys;

    /**暂时没有作用*/
    private String token;

    private Long loginTime;

    // 过期时间
    private Long expireTime;
    /**暂时没有作用*/

    public LoginAdmin(SysAdmin sysAdmin, List<SysRole> roles){
        this.authoritys = roles;
        this.sysAdmin = sysAdmin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的权限
        return this.authoritys;
    }

    @Override
    public String getPassword() {
        return this.sysAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return this.sysAdmin.getUsername();
    }

    // 是否过期
    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    // 是否锁定
    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    // 凭据是否过期
    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    // 是否禁用
    @Override
    public boolean isEnabled() {
        return false;
    }

    public SysAdmin getSysUser() {
        return sysAdmin;
    }

    public void setSysUser(SysAdmin sysAdmin) {
        this.sysAdmin = sysAdmin;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public List<SysRole> getAuthoritys() {
        return authoritys;
    }

    public void setAuthoritys(List<SysRole> authoritys) {
        this.authoritys = authoritys;
    }
}
