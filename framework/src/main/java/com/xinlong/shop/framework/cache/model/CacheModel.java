package com.xinlong.shop.framework.cache.model;

import java.io.Serializable;

/**
 * ehcache不能针对单个值做过期时间，所以用这个对象来做单个值过期
 * @Author Sylow
 * @Description
 * @Date: Created in 17:48 2023/1/4
 */
public class CacheModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Object value;

    /**
     * 过期时间 时间戳
     */
    private Long expirationTime;

    public CacheModel(){}

    public CacheModel(Object value, Long expirationTime) {
        this.value = value;
        this.expirationTime = expirationTime;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }
}
