package com.xinlong.shop.framework.uploader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 12:11 2022/11/18
 */
@Component
public class AliyunOSSConfig {

    @Value("${xinlong.aliyun.oss.endpoint}")
    private String endpoint;

    @Value("${xinlong.aliyun.oss.accessKeyId}")
    private String accessKeyId;

    @Value("${xinlong.aliyun.oss.access-key-secret}")
    private String accessKeySecret;

    @Value("${xinlong.aliyun.oss.bucketName}")
    private String bucketName;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
