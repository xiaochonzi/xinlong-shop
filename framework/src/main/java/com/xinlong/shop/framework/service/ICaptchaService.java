package com.xinlong.shop.framework.service;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:43 2023/1/1
 */
@Deprecated
public interface ICaptchaService {

    /**
     * 生成验证码
     * @param keyId
     * @return base64格式图片
     */
    String createCaptcha(String keyId);

    boolean verify(String code, String keyId);

}
