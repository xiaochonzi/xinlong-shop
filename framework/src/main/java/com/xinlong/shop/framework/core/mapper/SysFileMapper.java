package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysFile;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 系统上传文件表
 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-11-18
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFile> {

}
