package com.xinlong.shop.framework.weixin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:53 2023/1/3
 */
@Component
public class WeiXinMpConfig {

    @Value("${xinlong.weixin.mp.appid}")
    private String appid;

    @Value("${xinlong.weixin.mp.app-secret}")
    private String appSecret;


    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
