package com.xinlong.shop.framework.core.model;

import com.xinlong.shop.framework.core.entity.SysMenu;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 16:41 2022/7/22
 */
public class SysMenuDTO extends SysMenu {

    /**
     * 子菜单集合
     */
    private List<SysMenuDTO> children;

    public List<SysMenuDTO> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenuDTO> children) {
        this.children = children;
    }
}
