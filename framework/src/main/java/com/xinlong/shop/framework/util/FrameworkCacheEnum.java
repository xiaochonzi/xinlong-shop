package com.xinlong.shop.framework.util;

/**
 * Framework缓存枚举
 */
public enum FrameworkCacheEnum {

    ALL_MENU,   // 所有菜单
    ALL_ROLE,   // 所有角色
    ALL_GOODS_CLASSIFY,    // 所有商品分类

}
