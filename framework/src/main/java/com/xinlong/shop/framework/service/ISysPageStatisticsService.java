package com.xinlong.shop.framework.service;

import com.xinlong.shop.framework.core.entity.SysPageStatistics;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 页面统计 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-10
 */
public interface ISysPageStatisticsService extends IService<SysPageStatistics> {

    void update(SysPageStatistics sysPageStatistics, Integer id);

    void delete(Integer id);

    /**
     * 通过活动id和页面id 获取会员访问量
     * @param rushPurchaseId
     * @param pageId
     * @return
     */
    int visitNum(Integer rushPurchaseId, String pageId);
}
