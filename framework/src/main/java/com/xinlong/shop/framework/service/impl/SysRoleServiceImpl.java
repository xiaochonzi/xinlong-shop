package com.xinlong.shop.framework.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.framework.cache.ICache;
import com.xinlong.shop.framework.core.entity.SysRole;
import com.xinlong.shop.framework.core.mapper.SysRoleMapper;
import com.xinlong.shop.framework.service.ISysRoleService;
import com.xinlong.shop.framework.util.FrameworkCacheEnum;
import com.xinlong.shop.framework.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:53 2022/5/31
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {


    private final SysRoleMapper sysRoleMapper;

    private final ICache cache;

    public SysRoleServiceImpl(SysRoleMapper sysRoleMapper, ICache cache) {
        this.sysRoleMapper = sysRoleMapper;
        this.cache = cache;
    }

    @Override
    public List<SysRole> findAllRoles() {

        List<SysRole> allRole = (List<SysRole>) cache.get(FrameworkCacheEnum.ALL_ROLE.toString());

        if(allRole == null) {
            allRole = sysRoleMapper.findAllRoles();
            cache.put(FrameworkCacheEnum.ALL_ROLE.toString(), allRole);
        }

        return allRole;
    }

    @Override
    public List<SysRole> findRolesByAdminId(Integer adminId) {

        // 后期优化，把管理员的userid作为key存入缓存 管理员不多，也没事
        return sysRoleMapper.findRolesByAdminId(adminId);
    }

    @Override
    public List<SysRole> findRolesByPermission(String permission) {

        // 因为已经缓存所有的角色了，这里从缓存中拿即可
        List<SysRole> allRole = this.findAllRoles();
        List<SysRole> roles = new ArrayList<SysRole>();
        for(SysRole sysRole : allRole) {
            String permissions = sysRole.getPermissions();

            if (StringUtil.notEmpty(permissions) && permissions.indexOf(permission) != -1) {
                roles.add(sysRole);
            }

        }

        return roles;
    }

    @Override
    public void update(SysRole sysRole, Integer id) {
        UpdateWrapper<SysRole> sysRoleUpdateWrapper = new UpdateWrapper<>();
        sysRoleUpdateWrapper.eq("id", id);
        this.update(sysRole, sysRoleUpdateWrapper);

        this.onRoleChange();
    }

    @Override
    public void addSysRole(SysRole sysRole) {
        this.save(sysRole);

        this.onRoleChange();
    }

    @Override
    public void delete(Integer id) {

        UpdateWrapper<SysRole> sysRoleDeleteWrapper = new UpdateWrapper<>();
        sysRoleDeleteWrapper.eq("id",id);
        this.remove(sysRoleDeleteWrapper);

        this.onRoleChange();
    }

    @Override
    public void updatePermission(Integer id, String permissions) {
        UpdateWrapper<SysRole> sysRoleUpdateWrapper = new UpdateWrapper<>();
        sysRoleUpdateWrapper.set("permissions", permissions);
        sysRoleUpdateWrapper.eq("id", id);
        this.update(null, sysRoleUpdateWrapper);

        this.onRoleChange();
    }


    /**
     * 当角色发生变化时（增删改）统一方法
     */
    private void onRoleChange() {
        // 更新完成后，删除全部缓存，下次获取时自动更新
        cache.delete(FrameworkCacheEnum.ALL_ROLE.toString());
    }

}
