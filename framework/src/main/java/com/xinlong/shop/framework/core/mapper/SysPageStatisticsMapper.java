package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysPageStatistics;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 页面统计 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-10
 */
@Mapper
public interface SysPageStatisticsMapper extends BaseMapper<SysPageStatistics> {


    Integer rushPurchaseVisitNum(@Param("rushPurchaseId") Integer rushPurchaseId, @Param("pageId") String pageId);

}
