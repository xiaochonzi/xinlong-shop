package com.xinlong.shop.framework.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.framework.core.entity.SysMenu;
import com.xinlong.shop.framework.core.model.SysMenuDTO;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:07 2022/5/31
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<SysMenu> findAllMenus();

    List<SysMenuDTO> findMenusTree();

    void add(SysMenu sysMenu);

    void update(SysMenu sysMenu);

}
