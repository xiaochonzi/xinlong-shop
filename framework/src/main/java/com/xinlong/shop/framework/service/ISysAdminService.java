package com.xinlong.shop.framework.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.framework.core.entity.SysAdmin;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 01:27 2022/5/25
 */
public interface ISysAdminService extends IService<SysAdmin> {

    String login(String userName, String password);

    SysAdmin findByUserName(String userName);

    /**
     * 保存个人资料用
     * @param sysAdmin
     */
    void setUserInfo(SysAdmin sysAdmin);

    void update(SysAdmin sysAdmin, Integer id);

    void delete(Integer id);

    /**
     * 生成刷新token
     * @param userName
     * @param refreshToken 是否是refreshToken
     * @return
     */
    String generateRefreshToken(String userName, boolean refreshToken);

    /**
     * 刷新token
     * @param userName
     * @param refreshToken
     * @return
     */
//    String refreshToken(String userName, String refreshToken);

}
