package com.xinlong.shop.framework.setting;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 一些系统配置
 * @Author Sylow
 * @Description
 * @Date: Created in 21:32 2023/3/22
 */
@Component
public class XinLongConfig {
    @Value("${xinlong.domain.buyer}")
    private String buyerDomain;

    public String getBuyerDomain() {
        return buyerDomain;
    }

    public void setBuyerDomain(String buyerDomain) {
        this.buyerDomain = buyerDomain;
    }
}
