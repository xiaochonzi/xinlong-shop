package com.xinlong.shop.consumer.listener;

import com.rabbitmq.client.Channel;
import com.xinlong.shop.framework.rabbitmq.RabbitMqConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:10 2023/8/17
 */
@Component
public class TestListener {

    /**
     * 监听某个队列的消息
     * @param message 接收到的消息
     */
    @RabbitListener(queues = RabbitMqConfig.ITEM_QUEUE)
    public void myListener1(Message message, Channel channel) throws InterruptedException, IOException {
        System.out.println("消费者接收到的消息: " + new String(message.getBody(), "UTF-8"));
        Thread.sleep(5000);
        long tag = message.getMessageProperties().getDeliveryTag();
        System.out.println("消费完成：" + new String(message.getBody(), "UTF-8"));
        channel.basicAck(tag, true);
    }

}
