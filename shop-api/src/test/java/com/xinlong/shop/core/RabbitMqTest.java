//package com.xinlong.shop.core;
//
//import com.xinlong.shop.framework.core.entity.Member;
//import com.xinlong.shop.framework.rabbitmq.RabbitMqConfig;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
///**
// * @Author Sylow
// * @Description
// * @Date: Created in 21:25 2023/8/17
// */
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class RabbitMqTest {
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    // 生产者发送消息
//    @Test
//    public void testSendStr() {
//        /**
//         * 1. 参数一： 交换机名称
//         * 2. 参数二： routingKey
//         * 3. 参数三： 发送的消息
//         */
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.insert", "商品新增，routing key 为xinlong.insert");
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.update", "商品修改，routing key 为xinlong.update");
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.delete", "商品删除，routing key 为xinlong.delete");
//    }
//
//    // 生产者发送消息
//    @Test
//    public void testSendBean() {
//        /**
//         * 1. 参数一： 交换机名称
//         * 2. 参数二： routingKey
//         * 3. 参数三： 发送的消息
//         */
//        Member member = new Member();
//        member.setId(1);
//        member.setNickname("sylow");
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.member.add", member);
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.member.update", member);
//        rabbitTemplate.convertAndSend(RabbitMqConfig.ITEM_TOPIC_EXCHANGE, "xinlong.member.delete", member);
//    }
//}
