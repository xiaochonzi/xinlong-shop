package com.xinlong.shop.core;

import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.xinlong.shop.core.member.entity.MemberPayment;
import com.xinlong.shop.core.member.service.IMemberPaymentService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.core.entity.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 导入老系统会员测试用例
 * @Date: Created in 16:06 2023/4/19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ImportMemberTest {

    private static final Logger logger = LoggerFactory.getLogger(ImportMemberTest.class);

    @Resource
    private IMemberService memberService;

    @Resource
    private IMemberPaymentService memberPaymentService;

    @Test
    public void importTest() {

        List<Map<String, Object>> maps = SqlRunner.db().selectList("select * from tea_user");

        List<Member> memberList = new ArrayList<>();
        for(Map map : maps) {
            Member member = new Member();
            String phone = map.get("mobile").toString();
            String memberNo = "m_" + phone;

            String nickName;
            if (map.get("carName") == null) {
                nickName = memberNo;
            } else {
                nickName = map.get("carName").toString();
            }

            Integer inviteMemberId = Integer.parseInt(map.get("pid").toString());

            Long createTime = Long.parseLong(map.get("ctime").toString());

            BigDecimal balance = new BigDecimal(map.get("balance").toString());

            String password = new BCryptPasswordEncoder().encode("752513");

            member.setMemberNo(memberNo);
            member.setMemberName(memberNo);
            member.setPassword(password);
            member.setNickname( nickName);
            member.setMobile(phone);
            member.setCreateTime(createTime);
            member.setGradeId(1);
            member.setInviteMemberId(inviteMemberId);
            member.setBalance(balance);
            memberList.add(member);

        }
        memberService.saveBatch(memberList);


//         绑定邀请人
        memberList = memberService.list();
        for(Member member : memberList) {

            // 老系统中的邀请id
            Integer pid = member.getInviteMemberId();
            //System.out.println("当前寻找: " + member.getMobile());

            // 寻找老系统中的邀请人手机号
            for (Map user : maps) {
                Integer id = Integer.parseInt(user.get("id").toString());
                //System.out.println(id);
                //logger.debug("id:{}",id);
                if (pid.intValue() == id.intValue()) {
                    // 通过老系统中手机号 得到对应新系统会员id
                    String userMobile = user.get("mobile").toString();
                    System.out.println(userMobile);
                    Member inviteMember = memberService.findByMobile(userMobile);
                    member.setInviteMemberId(inviteMember.getId());
                    break;
                }
            }
        }
        memberService.saveOrUpdateBatch(memberList);

        List<MemberPayment> memberPayments = new ArrayList<>();
        // 遍历老系统会员
        for (Map user : maps) {
            // 通过老系统中手机号 得到对应新系统会员id
            String userMobile = user.get("mobile").toString();
            logger.info("userMobile:{}",userMobile);
            Member member = memberService.findByMobile(userMobile);

            // 新增银行卡
            if (user.get("carName") != null) {
                MemberPayment memberPayment = new MemberPayment();
                memberPayment.setMemberId(member.getId());
                memberPayment.setBankCode(user.get("bankCode").toString());
                memberPayment.setBankName(user.get("bankName").toString());
                memberPayment.setAccountName(user.get("carName").toString());
                memberPayment.setAccountNumber(user.get("carCode").toString());
                memberPayments.add(memberPayment);
            }
        }

        memberPaymentService.saveBatch(memberPayments);

    }

}
