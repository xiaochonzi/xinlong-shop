package com.xinlong.shop.api.common;

import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.exception.ServiceException;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;

/**
 * @Author Sylow
 * @Description 会员controller 基类
 * @Date: Created in 13:46 2023/4/7
 */
public class BuyerBaseController {

    protected final IMemberService memberService;
    protected final TokenUtil tokenUtil;

    public BuyerBaseController(IMemberService memberService, TokenUtil tokenUtil) {
        this.memberService = memberService;
        this.tokenUtil = tokenUtil;
    }

    protected Member getMember(String token) {
        token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
        String userName = tokenUtil.getUserNameFromToken(token);
        Member member = this.memberService.findMember(userName);
        if (member.getStatus().intValue() != 0) {
            throw new ServiceException("账号已被禁用");
        }
        return member;
    }
}
