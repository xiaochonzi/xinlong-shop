package com.xinlong.shop.api.common;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.blind.service.IBlindBoxGoodsService;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;
import com.xinlong.shop.core.goods.service.IGoodsService;
import com.xinlong.shop.core.site.entity.dto.HomeBlindBoxDTO;
import com.xinlong.shop.core.site.service.IHomeSiteBlindService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated      // get请求验证参数时需要
@RestController
@Api(tags = "买家首页位置API")
@RequestMapping("/buyer/home-site")
public class BuyerHomeSiteController {

    private final IHomeSiteBlindService homeSiteBlindService;
    private final IGoodsService goodsService;

    private final IBlindBoxGoodsService blindBoxGoodsService;

    public BuyerHomeSiteController(IHomeSiteBlindService homeSiteBlindService, IGoodsService goodsService, IBlindBoxGoodsService blindBoxGoodsService) {
        this.homeSiteBlindService = homeSiteBlindService;
        this.goodsService = goodsService;
        this.blindBoxGoodsService = blindBoxGoodsService;
    }

    @ApiOperation(value = "获取首页盲盒列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "siteCode", value = "位置编码", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "needChild", value = "是否需要显示盲盒内商品", required = false, paramType = "query", dataType = "Boolean"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = HomeBlindBoxDTO.class))
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public R list(@NotNull(message = "位置编码不能为空") String siteCode, Boolean needChild){
        List<HomeBlindBoxDTO> list = this.homeSiteBlindService.findBlindBoxList(siteCode);
        // 查询盲盒内的商品明细
        if (needChild != null && needChild) {
            // 这个方案只是暂时的 而且首页数据不多 也就四五个 所以暂定循环查库
            list.forEach(homeBlindBoxDTO -> {
                homeBlindBoxDTO.setBlindBoxGoodsList(this.blindBoxGoodsService.findByBlindBoxId(homeBlindBoxDTO.getId()));
            });
        }
        return R.success("操作成功", list);
    }

    @ApiOperation(value = "获取首页展示的商品列表，按照最新发布的前5个")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = GoodsDTO.class))
    @RequestMapping(value = "/goods-list", method = RequestMethod.GET, produces = "application/json")
    public R shopList() {
        IPage<GoodsDTO> page = new Page<>(1, 5);
        GoodsQueryParam goodsQueryParam = new GoodsQueryParam();
        goodsQueryParam.setSortName("create_time");
        goodsQueryParam.setSortType("desc");
        page = this.goodsService.page(page, goodsQueryParam);
        List<GoodsDTO> list = page.getRecords();
        return R.success(list);
    }

}
