package com.xinlong.shop.api.common;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.service.ICaptchaService;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * 验证码
 * @Author Sylow
 * @Description 20230217: 这个代码过期了，要改缓存部分,目前前端因为工期原因 暂时不做验证码
 * @Date: Created in 22:59 2023/1/1
 */
@Deprecated
@RestController
@RequestMapping("/buyer/captcha")
public class BuyerCaptchaController {

    private final ICaptchaService captchaService;

    public BuyerCaptchaController(ICaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    // 这里key从前端传递过来 可能会有问题
    @GetMapping("")
    public R index(String keyId) {
        // 有值就是重新生成
        if (StrUtil.isBlank(keyId)) {
            keyId = IdUtil.simpleUUID();
        }
        String base64 = captchaService.createCaptcha(keyId);
        Map<String, Object> map = new HashMap();
        map.put("keyId", keyId);
        map.put("captcha", base64);

        return R.success(map);
    }

    @GetMapping("/verify")
    public R verify(String keyId, String code) {
        boolean result = captchaService.verify(code, keyId);
        return R.success(result);
    }

}
