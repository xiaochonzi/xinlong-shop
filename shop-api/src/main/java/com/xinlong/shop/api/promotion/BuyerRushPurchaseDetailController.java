package com.xinlong.shop.api.promotion;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.promotion.entity.DetailStatusEnum;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseDetailDTO;
import com.xinlong.shop.core.promotion.service.IRushPurchaseCartService;
import com.xinlong.shop.core.promotion.service.IRushPurchaseDetailService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import com.xinlong.shop.framework.util.StringUtil;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 抢购活动详情
 * </p>
 *
 * @author Sylow
 * @since 2023-03-07
 */
@RestController
@RequestMapping("/buyer/promotion/rush-purchase-detail")
public class BuyerRushPurchaseDetailController extends BuyerBaseController {

    private final IRushPurchaseDetailService rushPurchaseDetailService;
    private final IRushPurchaseCartService rushPurchaseCartService;
    private final IMemberService memberService;


    public BuyerRushPurchaseDetailController(IRushPurchaseDetailService rushPurchaseDetailService, IRushPurchaseCartService rushPurchaseCartService, IMemberService memberService, TokenUtil tokenUtil, IMemberService memberService1) {
        super(memberService, tokenUtil);
        this.rushPurchaseDetailService = rushPurchaseDetailService;
        this.rushPurchaseCartService = rushPurchaseCartService;
        this.memberService = memberService1;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, Integer rushPurchaseId, Integer min, Integer max){
        IPage<RushPurchaseDetailDTO> page = new Page<>(current, size);
//        QueryWrapper query = new QueryWrapper();
//        query.eq("rush_purchase_id", rushPurchaseId);
        page = this.rushPurchaseDetailService.page(page, rushPurchaseId, min, max);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public R rushPurchaseDetail(@PathVariable("id") Integer rushPurchaseDetailId){
        RushPurchaseDetailDTO rushPurchaseDetailDTO = this.rushPurchaseDetailService.findDTOById(rushPurchaseDetailId);
        return R.success("操作成功", rushPurchaseDetailDTO);
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public R buy(Integer rushPurchaseDetailId, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        if (rushPurchaseDetailId == null) {
            return R.error("活动商品不能为空");
        }
        boolean cartHave = this.rushPurchaseCartService.isHave(rushPurchaseDetailId);
        if (cartHave) {
            return R.success("操作成功","已被其他用户抢购");
        }
        Member member = getMember(token);
        String result = this.rushPurchaseDetailService.buy(rushPurchaseDetailId, member);

        return R.success("操作成功", result);
    }

    @RequestMapping(value = "/buyer-order", method = RequestMethod.GET)
    public R buyerOrder(Integer current, String status, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        Integer statusIndex = null;
        if ("buy".equals(status)) {
            statusIndex = DetailStatusEnum.BUY.getCode();
        } else if("pay".equals(status)) {
            statusIndex = DetailStatusEnum.PAY.getCode();
        } else if("confirm".equals(status)) {
            statusIndex = DetailStatusEnum.CONFIRM.getCode();
        } else if("put-sale".equals(status)) {
            statusIndex = DetailStatusEnum.PUT_SALE.getCode();
        }
        Member member = getMember(token);
        IPage<RushPurchaseDetailDTO> page = new Page<>(current, 10);
        page = this.rushPurchaseDetailService.findBuyerOrder(page, member.getId(), statusIndex);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/upload-proof", method = RequestMethod.POST)
    public R uploadProof(String proofUrl, Integer id, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        if (StrUtil.isBlank(proofUrl)) {
            return R.error("凭证不能为空");
        }
        Member member = getMember(token);
        this.rushPurchaseDetailService.uploadProof(id, member.getId(), proofUrl);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/seller-order", method = RequestMethod.GET)
    public R sellerOrder(Integer current, String status, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        Integer statusIndex = null;
        if ("not-buy".equals(status)) {
            statusIndex = DetailStatusEnum.NOT_BUY.getCode();
        } else if("buy".equals(status)) {
            statusIndex = DetailStatusEnum.BUY.getCode();
        } else if("pay".equals(status)) {
            statusIndex = DetailStatusEnum.PAY.getCode();
        } else if("confirm".equals(status)) {
            statusIndex = DetailStatusEnum.CONFIRM.getCode();
        }
        Member member = getMember(token);
        IPage<RushPurchaseDetailDTO> page = new Page<>(current, 10);
        page = this.rushPurchaseDetailService.findSellerOrder(page, member.getId(), statusIndex);
        return R.success("操作成功", page);
    }

    //确认收款
    @RequestMapping(value = "/confirm-receipt", method = RequestMethod.POST)
    public R confirmReceipt(Integer id, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        if (id == 0) {
            return R.error("id不能为空");
        }
        Member member = getMember(token);
        RushPurchaseDetail rushPurchaseDetail = this.rushPurchaseDetailService.getById(id);
        if (rushPurchaseDetail == null) {
            return R.error("活动商品不存在");
        }
        if (!member.getId().equals(rushPurchaseDetail.getSellerId())) {
            return R.error("非法操作");
        }
        this.rushPurchaseDetailService.confirmReceipt(id);
        return R.success("操作成功");
    }

    //确认提货
    @RequestMapping(value = "/pick-up", method = RequestMethod.POST)
    public R pickUp(Integer id, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        if (id == 0) {
            return R.error("id不能为空");
        }
        Member member = getMember(token);
        this.rushPurchaseDetailService.pickUp(id, member.getId());
        return R.success("操作成功");
    }

    @RequestMapping(value = "/set-booking-member", method = RequestMethod.PUT)
    public R setBookingMember(Integer rushPurchaseDetailId, String mobile) {
        if (!StringUtil.vaildMobile(mobile)) {
            return R.error("手机号格式不正确");
        }
        Member member = this.memberService.findByMobile(mobile);
        if (member == null) {
            return R.error("未找到此用户");
        }
        this.rushPurchaseDetailService.setBookingMember(rushPurchaseDetailId, member.getId());
        return R.success();
    }

    @RequestMapping(value = "/del-booking-member", method = RequestMethod.DELETE)
    public R delBookingMember(Integer rushPurchaseDetailId) {
        // 设置为0 就是删除
        this.rushPurchaseDetailService.setBookingMember(rushPurchaseDetailId, 0);
        return R.success();
    }


}
