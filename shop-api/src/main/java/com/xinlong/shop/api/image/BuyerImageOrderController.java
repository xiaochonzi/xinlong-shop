//package com.xinlong.shop.api.image;
//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.xinlong.shop.api.common.BuyerBaseController;
//import com.xinlong.shop.core.blind.entity.BlindBoxOrder;
//import com.xinlong.shop.core.image.entity.ImageOrder;
//import com.xinlong.shop.core.image.service.IImageOrderService;
//import com.xinlong.shop.core.member.service.IMemberService;
//import com.xinlong.shop.framework.common.R;
//import com.xinlong.shop.framework.core.entity.Member;
//import com.xinlong.shop.framework.core.entity.SysFile;
//import com.xinlong.shop.framework.security.constant.SecurityConstants;
//import com.xinlong.shop.framework.security.util.TokenUtil;
//import com.xinlong.shop.framework.service.ISysFileService;
//import com.xinlong.shop.framework.util.FileUtils;
//import com.xinlong.shop.framework.weixin.service.WeiXinMpService;
//import io.swagger.annotations.*;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.validation.constraints.NotBlank;
//
///**
// * @Author Sylow
// * @Description 图片处理订单API
// * @Date: Created in 03:29 2023/8/4
// */
//@Validated      // get请求验证参数时需要
//@RestController
//@Api(tags = "买家图片处理订单API")
//@RequestMapping("/buyer/image-order")
//public class BuyerImageOrderController extends BuyerBaseController {
//
//    private final WeiXinMpService weiXinMpService;
//    private final IImageOrderService imageOrderService;
//    private final ISysFileService sysFileService;
//
//    public BuyerImageOrderController(IMemberService memberService, TokenUtil tokenUtil, WeiXinMpService weiXinMpService, IImageOrderService imageOrderService, ISysFileService sysFileService) {
//        super(memberService, tokenUtil);
//        this.weiXinMpService = weiXinMpService;
//        this.imageOrderService = imageOrderService;
//        this.sysFileService = sysFileService;
//    }
//
//
//    @ApiOperation(value = "创建图片处理订单")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "imageUrl", value = "图片地址", required = true, paramType = "query"),
//    })
//    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = BlindBoxOrder.class))
//    @RequestMapping(value = "/create-order", method = RequestMethod.POST, produces = "application/json")
//    public R createOrder(@NotBlank(message = "图片不能为空") String imageUrl, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){       // 1. 验证token
//        try {
//            Member member = getMember(token);
//            if (member == null) {
//                R.error("请先登录");
//            }
//            if (member.getStatus().intValue() != 0) {
//                R.error("账号已被禁用");
//            }
//            if (member.getOpenId() == null || "".equals(member.getOpenId())) {
//                R.error("请先绑定微信");
//            }
//
//            MultipartFile file = FileUtils.uploadImgUrlToMultipartFile(imageUrl);
//            SysFile sysFile = sysFileService.upload(file, "image");
//            ImageOrder order = this.imageOrderService.createOrder(member.getId(), sysFile.getUrl());
//
//            // 虚拟商品不支持购买  暂时注释
////            PrepayWithRequestPaymentResponse response = weiXinMpService.miniPay(order.getOrderSn(), member.getOpenId(),
////                    order.getPrice(), "图片处理服务: " + order.getOrderSn());
//            this.memberService.subPoints(member.getId(), order.getPrice(), 1, order.getOrderSn(), "购买图片处理服务");
//            this.imageOrderService.paySuccess(order.getOrderSn(), "no pay sn");
//            return R.success();
//        } catch (Exception e){
//            return R.error(e.getMessage());
//        }
//    }
//
//    @ApiOperation(value = "图片订单列表")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "current", value = "当前页", required = true, paramType = "query"),
//    })
//    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = ImageOrder.class))
//    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
//    public R orderList(Integer current, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
//        Member member = getMember(token);
//        if (member == null) {
//            R.error("请先登录");
//        }
//        IPage<ImageOrder> page = new Page<>(current, 10);
//        page = this.imageOrderService.selectPage(page, member.getId());
//        return R.success(page);
//    }
//}
