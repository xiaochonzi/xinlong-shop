package com.xinlong.shop.api.member;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.entity.MemberActivityIncome;
import com.xinlong.shop.core.member.service.IMemberActivityIncomeService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.promotion.service.IRushPurchaseDetailService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 会员收益表 前端控制器
 * </p>
 * 2023 0330: 管理界面待对接
 * @author Sylow
 * @since 2023-03-30
 */
@RestController
@RequestMapping("/buyer/member/activity-income")
public class BuyerActivityIncomeControllerBuyer extends BuyerBaseController {

    private final IMemberActivityIncomeService memberActivityIncomeService;
    private final IRushPurchaseDetailService rushPurchaseDetailService;

    public BuyerActivityIncomeControllerBuyer(IMemberActivityIncomeService memberActivityIncomeService, IMemberService memberService, TokenUtil tokenUtil, IRushPurchaseDetailService rushPurchaseDetailService) {
        super(memberService, tokenUtil);
        this.memberActivityIncomeService = memberActivityIncomeService;

        this.rushPurchaseDetailService = rushPurchaseDetailService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        IPage<MemberActivityIncome> page = new Page<>(current, size);
        Member member = getMember(token);
        page = this.memberActivityIncomeService.page(page, member.getId());
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public R total(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        Member member = getMember(token);
        BigDecimal total = this.memberActivityIncomeService.getTotal(member.getId());
        return R.success("操作成功", total);
    }

    @RequestMapping(value = "/total/info", method = RequestMethod.GET)
    public R month(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        Member member = getMember(token);
        // 月收益
        Long startTime = DateUtil.beginOfMonth(new Date()).getTime() / 1000;
        Long endTime = DateUtil.endOfMonth(new Date()).getTime() / 1000;
        BigDecimal monthTotal = this.memberActivityIncomeService.getTotalByTime(member.getId(), startTime, endTime);

        // 日收益
        startTime = DateUtil.beginOfDay(new Date()).getTime() / 1000;
        endTime = DateUtil.endOfDay(new Date()).getTime() / 1000;
        BigDecimal dayTotal = this.memberActivityIncomeService.getTotalByTime(member.getId(), startTime, endTime);
        Map<String, Object> result = new HashMap<>();

        // 预期收益
        BigDecimal expectIncome = this.rushPurchaseDetailService.getExpectIncome(member.getId());

        result.put("monthTotal", monthTotal);
        result.put("dayTotal", dayTotal);
        result.put("expectIncome", expectIncome);

        return R.success("操作成功", result);
    }




}
