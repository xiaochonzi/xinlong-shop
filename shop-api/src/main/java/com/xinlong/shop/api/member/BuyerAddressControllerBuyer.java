package com.xinlong.shop.api.member;

import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.address.entity.MemberAddress;
import com.xinlong.shop.core.address.service.IMemberAddressService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import com.xinlong.shop.framework.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 会员收货地址 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@RestController
@RequestMapping("/buyer/member/address")
public class BuyerAddressControllerBuyer extends BuyerBaseController {

    private final IMemberAddressService memberAddressService;

    public BuyerAddressControllerBuyer(IMemberAddressService memberAddressService, IMemberService memberService, TokenUtil tokenUtil) {
        super(memberService, tokenUtil);
        this.memberAddressService = memberAddressService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            List<MemberAddress> list = memberAddressService.listByMemberId(memberId);
            return R.success("操作成功", list);
        }
        return R.error("未登录");
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody MemberAddress memberAddress, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            memberAddress.setMemberId(memberId);
            this.memberAddressService.saveAddress(memberAddress);
            return R.success("操作成功");
        }

        return R.error("未登录");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberAddress memberAddress, @PathVariable("id") Integer id,@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            memberAddress.setMemberId(memberId);
            this.memberAddressService.update(memberAddress, id);
            return R.success("操作成功");
        }

        return R.error("未登录");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            this.memberAddressService.delete(id, memberId);
            return R.success("操作成功");
        }
        return R.error("未登录");
    }

    /**
     * 获取会员地址数量 用于判断能否参与抢购
     * @param memberId
     * @return
     */
    @RequestMapping(value = "/num", method = RequestMethod.GET)
    public R addressNum(Integer memberId){
        if (memberId != null) {
            long num = this.memberAddressService.findAddressNum(memberId);
            return R.success("操作成功", num);
        }
        return R.error("会员id不能为空");
    }

    @ApiOperation(value = "获取默认地址")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = MemberAddress.class))
    @RequestMapping(value = "/def-address", method = RequestMethod.GET, produces = "application/json")
    public R getDefAddress(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        Member member = getMember(token);
        MemberAddress address = this.memberAddressService.getDefByMemberId(member.getId());
        return R.success("操作成功", address);
    }

}
