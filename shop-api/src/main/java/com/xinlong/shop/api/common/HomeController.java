package com.xinlong.shop.api.common;

import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 16:21 2022/11/30
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @GetMapping("/index")
    public R index() {
        String goodsName = "商品名称";
        return R.success("操作成功", goodsName);
    }
}
