## Xinlong-shop 信隆商城系统

#### 项目简介

本开源项目持续更新中..... 只要作者还有饭吃就还会更新（手动狗头）

##### 初衷

我的职业生涯面对的商城系统都是一些大型系统，使用的技术栈太重、中间件太多、上手太复杂，这种适合大型项目。所以之前有不少客户一直问我 有没有那种简单容易上手的，所以业余时间我写了一个特别简单的，一方面重新锻炼自己的开发能力，另一方面为初学者提供简单易上手的系统

##### 技术栈

###### 后端

Springboot2、JWT、Ehcache、MySQL

###### 管理端UI

Vue3、Naive Ui Admin

###### 移动端

Uniapp、Vue3、Vite、图鸟UI

##### 运行环境

Jdk1.8、Maven、MySQL5.7、Uniapp、NPM（10.1.0）、Node.js（20.8.0）、Yarn（1.22.19）

###### 开发工具

IDEA、VScode

#### 演示站

上次更新：2023年10月10日

后台管理：http://admin.xinlongtech.com.cn     admin/111111

移动端：http://shop.xinlongtech.com.cn     15911100004/111111

移动端短信正常发送，也可自行注册

#### 项目源码地址

Java后端：https://gitee.com/xinlong-youchuang/xinlong-shop


配置中心：https://gitee.com/xinlong-youchuang/xinlong-shop-config

前端管理端UI：https://gitee.com/xinlong-youchuang/xinlong-ui

移动端：https://gitee.com/xinlong-youchuang/xinlong-shop-mobile

#### 本地运行

##### 后端篇

1. 请确保Jdk、maven、MySQL正常安装

2. 克隆 Java代码和配置中心

​	`git clone https://gitee.com/xinlong-youchuang/xinlong-shop `

​	`	git clone https://gitee.com/xinlong-youchuang/xinlong-shop-config`

3. 修改配置文件路径

​	config-server工程中`src/main/resources/application.yml`文件

<img src="https://xinlong-shop.oss-cn-beijing.aliyuncs.com/static/docs/image-20231011103450602.png" alt="image-20231011103450602" style="zoom:50%;" />

​	把这个路径改为你的config项目路径

4. 修改配置文件内容

​	如果只是本地运行，那修改MySQL配置就行，把weixin-pay: true改为false，这样就不会加载微信配置文件了

​	数据库sql在项目根目录的sql，直接导入即可

5. 运行（Java）
   1. 运行config-server
   2. 验证：http://localhost:8888/shop-api/dev
   
      如果出现了你的配置，那就正确了
   3. 运行shop-api

##### 管理端UI

1. 请先安装好npm和node，yarn用不用都行，我是习惯性用yarn

2. 克隆代码

   `git clone https://gitee.com/xinlong-youchuang/xinlong-ui`

3. 配置

   默认.env.development文件是配置的localhost，如果你需要修改 在里面修改API接口地址即可

4. 运行

   `yarn install`

   `yarn run dev`

##### 移动端

1. 管理端环境搭建好之后，用同样的环境即可

2. 克隆代码

   `https://gitee.com/xinlong-youchuang/xinlong-shop-mobile`

3. 配置config

   /src/config/index.ts文件 检查env值是否为dev，然后把api_env和domain_env值改为正确值

   api_env 就是后端的API地址、domain_env就是移动端运行地址

4. 运行

   `yarn install`

   `yarn run dev:h5`

#### 线上运行

Java打成jar包就行，前端build成html，配合nginx做反向代理即可，目前应该不会有人用吧，有需要的话联系我，我已经写好脚本了

#### 系统功能

##### 平台管理端功能

| 模块      | 功能                                                       |
| --------- | ---------------------------------------------------------- |
| Dashboard | 工作台、快捷操作                                           |
| 会员管理  | 会员列表、签名管理（待废弃）、会员等级、提现管理、余额记录 |
| 商品管理  | 商品列表、商品分类、商品品牌                               |
| 盲盒管理  | 盲盒列表、盲盒订单                                         |
| 订单管理  | 订单列表、发货                                             |
| 促销活动  | 抢购活动、抢购订单、上架订单（这个模块待废弃）             |
| 站点管理  | 首页管理、广告管理、物流管理、文章管理                     |
| 系统设置  | 菜单管理、角色权限管理、管理员管理                         |
| 设置页面  | 系统设置、个人设置                                         |

##### 功能动图展示

<img src="https://xinlong-shop.oss-cn-beijing.aliyuncs.com/static/docs/%E7%AE%A1%E7%90%86%E7%AB%AF%E5%B1%95%E7%A4%BA.gif" alt="管理端展示" style="zoom:50%;" />

<img src="https://xinlong-shop.oss-cn-beijing.aliyuncs.com/static/docs/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E5%B1%95%E7%A4%BA.gif" alt="移动端展示" style="zoom:50%;" />



#### 交流

微信：
![weixin](https://www.xinlongtech.com.cn/images/weixin-erweima.jpg)