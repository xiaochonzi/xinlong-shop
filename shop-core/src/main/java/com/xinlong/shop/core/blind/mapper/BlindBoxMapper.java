package com.xinlong.shop.core.blind.mapper;

import com.xinlong.shop.core.blind.entity.BlindBox;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 盲盒 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@Mapper
public interface BlindBoxMapper extends BaseMapper<BlindBox> {

}
