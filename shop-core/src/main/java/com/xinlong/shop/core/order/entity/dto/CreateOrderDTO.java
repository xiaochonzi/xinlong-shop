package com.xinlong.shop.core.order.entity.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author Sylow
 * @Description 创建订单DTO
 * @Date: Created in 15:37 2023/7/4
 */
@Api(value = "创建订单DTO")
public class CreateOrderDTO {

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 会员名
     */
    private String memberName;

    @ApiModelProperty(value = "购买商品ids")
    @NotEmpty(message = "购买商品ids不能为空")
    private List<Integer> goodsIds;

    @ApiModelProperty(value = "配送区域id")
    @NotBlank(message = "配送区域id不能为空")
    private Integer shipAreaId;

    @ApiModelProperty(value = "配送区域名称")
    @NotBlank(message = "配送区域名称不能为空")
    private String shipAreaName;

//    /**
//     * 配送省份id
//     */
//    @ApiModelProperty(value = "配送省份id")
//    @NotBlank(message = "配送省份id不能为空")
//    private Integer shipProvinceId;
//
//    /**
//     * 配送省名
//     */
//    @ApiModelProperty(value = "配送省名")
//    @NotBlank(message = "配送省名不能为空")
//    private String shipProvinceName;
//
//    /**
//     * 配送市区id
//     */
//    @ApiModelProperty(value = "配送市区id")
//    @NotBlank(message = "配送市区id不能为空")
//    private Integer shipCityId;
//
//    /**
//     * 配送市区名称
//     */
//    @ApiModelProperty(value = "配送市区名称")
//    @NotBlank(message = "配送市区名称不能为空")
//    private String shipCityName;
//
//    /**
//     * 配送县区id
//     */
//    @ApiModelProperty(value = "配送县区id")
//    @NotBlank(message = "配送县区id不能为空")
//    private Integer shipCountyId;
//
//    /**
//     * 配送县区名称
//     */
//    @ApiModelProperty(value = "配送县区名称")
//    @NotBlank(message = "配送县区名称不能为空")
//    private String shipCountyName;

    /**
     * 配送详细地址
     */
    @ApiModelProperty(value = "配送详细地址")
    @NotBlank(message = "配送详细地址不能为空")
    private String shipFullAddress;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(value = "收货人姓名")
    @NotBlank(message = "收货人姓名不能为空")
    private String shipName;

    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号")
    @NotBlank(message = "收货人手机号不能为空")
    private String shipMobile;

    public List<Integer> getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(List<Integer> goodsIds) {
        this.goodsIds = goodsIds;
    }

    public Integer getShipAreaId() {
        return shipAreaId;
    }

    public void setShipAreaId(Integer shipAreaId) {
        this.shipAreaId = shipAreaId;
    }

    public String getShipAreaName() {
        return shipAreaName;
    }

    public void setShipAreaName(String shipAreaName) {
        this.shipAreaName = shipAreaName;
    }

    public String getShipFullAddress() {
        return shipFullAddress;
    }

    public void setShipFullAddress(String shipFullAddress) {
        this.shipFullAddress = shipFullAddress;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
}
