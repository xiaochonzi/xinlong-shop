package com.xinlong.shop.core.promotion.mapper;

import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 抢购活动表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
@Mapper
public interface RushPurchaseMapper extends BaseMapper<RushPurchase> {

}
