package com.xinlong.shop.core.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Sylow
 * @Description 活动商品详情Mapper
 * @Date: Created in 12:45 2023/3/8
 */
@Mapper
public interface RushPurchaseDetailMapper extends BaseMapper<RushPurchaseDetail> {

}
