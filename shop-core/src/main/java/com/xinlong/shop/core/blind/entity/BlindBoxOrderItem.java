package com.xinlong.shop.core.blind.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 盲盒订单项
 * </p>
 *
 * @author Sylow
 * @since 2023-06-27
 */
@ApiModel(value="BlindBoxOrderItem对象", description="盲盒订单项")
@TableName("s_blind_box_order_item")
public class BlindBoxOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 盲盒订单id
     */
    private Integer blindBoxOrderId;

    /**
     * 盲盒商品id
     */
    private Integer blindBoxId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    /**
     * 商品主图
     */
    @ApiModelProperty(value = "商品主图")
    private String goodsImg;

    /**
     * 商品价格
     */
    @ApiModelProperty(value = "商品价格")
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    private Integer num;

    /**
     * 发货订单编号
     */
    @ApiModelProperty(value = "发货订单编号")
    private String shipOrderSn;

    /**
     * 状态（未提货=0、已提货=1）
     */
    @ApiModelProperty(value = "状态（未提货=0、已提货=1）")
    private Integer status;

    /**
     * 提货时间
     */
    @ApiModelProperty(value = "提货时间")
    private Long pickupTime;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getBlindBoxOrderId() {
        return blindBoxOrderId;
    }

    public void setBlindBoxOrderId(Integer blindBoxOrderId) {
        this.blindBoxOrderId = blindBoxOrderId;
    }
    public Integer getBlindBoxId() {
        return blindBoxId;
    }

    public void setBlindBoxId(Integer blindBoxId) {
        this.blindBoxId = blindBoxId;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getShipOrderSn() {
        return shipOrderSn;
    }

    public void setShipOrderSn(String shipOrderSn) {
        this.shipOrderSn = shipOrderSn;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Long pickupTime) {
        this.pickupTime = pickupTime;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "BlindBoxOrderItem{" +
            "id=" + id +
            ", goodsId=" + goodsId +
            ", memberId=" + memberId +
            ", blindBoxOrderId=" + blindBoxOrderId +
            ", blindBoxId=" + blindBoxId +
            ", goodsName=" + goodsName +
            ", goodsImg=" + goodsImg +
            ", goodsPrice=" + goodsPrice +
            ", num=" + num +
            ", shipOrderSn=" + shipOrderSn +
            ", status=" + status +
            ", pickupTime=" + pickupTime +
            ", createTime=" + createTime +
        "}";
    }
}
