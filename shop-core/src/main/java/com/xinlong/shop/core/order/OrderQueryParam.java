package com.xinlong.shop.core.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author Sylow
 * @Description 订单查询参数
 * @Date: Created in 13:29 2023/7/4
 */
@ApiModel(value="OrderQueryParam对象", description="订单查询参数")
public class OrderQueryParam {

    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "订单类型")
    private Integer orderType;

    @ApiModelProperty(value = "会员手机号")
    private String memberMobile;

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "会员id")
    private Integer memberId;

    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间")
    private Long[] buyTime;

    /**
     * 排序字段名称
     */
    @ApiModelProperty(value = "排序字段名称")
    private String sortName = "create_time";

    /**
     * 排序类型 desc asc
     */
    @ApiModelProperty(value = "排序类型 desc asc")
    private String sortType = "desc";

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long[] getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Long[] buyTime) {
        this.buyTime = buyTime;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
}