package com.xinlong.shop.core.member.mapper;

import com.xinlong.shop.core.member.entity.MemberSign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 会员签名记录表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@Mapper
public interface MemberSignMapper extends BaseMapper<MemberSign> {

}
