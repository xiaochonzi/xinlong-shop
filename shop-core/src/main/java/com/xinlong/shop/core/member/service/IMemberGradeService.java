package com.xinlong.shop.core.member.service;

import com.xinlong.shop.core.member.entity.MemberGrade;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 会员等级 服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
public interface IMemberGradeService extends IService<MemberGrade> {

    void update(MemberGrade memberGrade, Integer id);

    void delete(Integer id);

    List<MemberGrade> list();
}
