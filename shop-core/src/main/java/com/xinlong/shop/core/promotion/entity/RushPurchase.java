package com.xinlong.shop.core.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 抢购活动表
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
@TableName("s_rush_purchase")
public class RushPurchase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主健
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动场次id
     */
    private String activityId;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 上一场活动id 自动生成的活动才会有
     */
    private Integer lastId;

    /**
     * 活动状态：正常/关闭
     */
    private Integer status;

    @TableField(exist = false)
    private int visitNum;

    @TableField(exist = false)
    private BigDecimal totalPrice;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getActivityName() {
        return activityName;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getVisitNum() {
        return visitNum;
    }

    public void setVisitNum(int visitNum) {
        this.visitNum = visitNum;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "RushPurchase{" +
            "id=" + id +
            ", activityName=" + activityName +
            ", activityId=" + activityId +
            ", startTime=" + startTime +
            ", endTime=" + endTime +
            ", createTime=" + createTime +
            ", lastId=" + lastId +
            ", status=" + status +
        "}";
    }
}
