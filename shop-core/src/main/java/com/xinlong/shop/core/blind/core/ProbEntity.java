package com.xinlong.shop.core.blind.core;

/**
 * @Author Sylow
 * @Description 概率实体，用于盲盒开盒
 * @Date: Created in 17:27 2023/6/6
 */
public class ProbEntity {

    /**
     * id 有可能是string也有可能是数字
     */
    private Object id;
    /**
     * 概率（权重）
     */
    private Integer rate;


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
