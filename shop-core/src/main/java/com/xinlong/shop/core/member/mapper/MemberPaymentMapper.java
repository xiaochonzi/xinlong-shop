package com.xinlong.shop.core.member.mapper;

import com.xinlong.shop.core.member.entity.MemberPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 会员收款方式 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-03-15
 */
@Mapper
public interface MemberPaymentMapper extends BaseMapper<MemberPayment> {

}
