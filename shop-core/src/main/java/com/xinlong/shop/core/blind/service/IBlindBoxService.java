package com.xinlong.shop.core.blind.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxVO;

import java.util.List;

/**
 * <p>
 * 盲盒 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
public interface IBlindBoxService extends IService<BlindBox> {

    void update(BlindBox blindBox, Integer id);

    void delete(Integer id);

    /**
     * 保存盲盒商品活动
     * @param blindBoxVO
     */
    void saveBlindBox(BlindBoxVO blindBoxVO);

    /**
     * 修改盲盒商品活动
     * @param blindBoxVO
     * @param id
     */
    void updateBlindBox(BlindBoxVO blindBoxVO, Integer id);

    void updateStatus(Integer id, Integer status);

    List<BlindBox> findAllBlindBox(String blindBoxName);

}
