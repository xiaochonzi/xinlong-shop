package com.xinlong.shop.core.address.service;

import com.xinlong.shop.core.address.entity.SysArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地区表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
public interface ISysAreaService extends IService<SysArea> {

    void update(SysArea sysArea, Integer id);

    void delete(Integer id);
}
