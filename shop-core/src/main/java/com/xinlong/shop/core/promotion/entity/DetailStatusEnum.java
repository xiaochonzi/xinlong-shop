package com.xinlong.shop.core.promotion.entity;

public enum DetailStatusEnum {
    // 未采购、已采购(待支付)、已支付(待确认收款)、已确认收款(待上架)、已上架
    NOT_BUY(0),BUY(1),PAY(2),CONFIRM(3),PUT_SALE(4),CANCEL(5);

    private int code;
    private DetailStatusEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }

}
