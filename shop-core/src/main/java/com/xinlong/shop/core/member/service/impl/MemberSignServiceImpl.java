package com.xinlong.shop.core.member.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.member.entity.MemberSign;
import com.xinlong.shop.core.member.mapper.MemberSignListDTOMapper;
import com.xinlong.shop.core.member.mapper.MemberSignMapper;
import com.xinlong.shop.core.member.service.IMemberSignService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 会员签名记录表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@Service
public class MemberSignServiceImpl extends ServiceImpl<MemberSignMapper, MemberSign> implements IMemberSignService {

    private final MemberSignListDTOMapper memberSignListDTOMapper;

    public MemberSignServiceImpl(MemberSignListDTOMapper memberSignListDTOMapper) {
        this.memberSignListDTOMapper = memberSignListDTOMapper;
    }

    @Override
    public void update(MemberSign memberSign, Integer id) {
        UpdateWrapper<MemberSign> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberSign, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberSign> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public IPage page(IPage page, Integer status) {
        return this.memberSignListDTOMapper.findPage(page, status);
    }

    @Override
    public void pass(Integer id) {
        UpdateWrapper<MemberSign> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        updateWrapper.set("status", 1);
        this.update(updateWrapper);
    }

    @Override
    public MemberSign findByMemberId(Integer memberId) {
        QueryWrapper<MemberSign> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        List<MemberSign> list = this.list(query);
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void addSign(Integer memberId, String signImg) {
        // 如果有了就重签
        MemberSign memberSign = findByMemberId(memberId);
        if (memberSign != null) {
            memberSign.setSignImg(signImg);
            memberSign.setSignTime(DateUtil.currentSeconds());
            this.update(memberSign, memberSign.getMemberId());
        } else {
            memberSign = new MemberSign();
            memberSign.setMemberId(memberId);
            memberSign.setSignImg(signImg);
            memberSign.setSignTime(DateUtil.currentSeconds());
            this.save(memberSign);
        }
    }

}
