package com.xinlong.shop.core.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.member.entity.MemberPayment;
import com.xinlong.shop.core.member.mapper.MemberPaymentMapper;
import com.xinlong.shop.core.member.service.IMemberPaymentService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.core.entity.Member;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收款方式 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-15
 */
@Service
public class MemberPaymentServiceImpl extends ServiceImpl<MemberPaymentMapper, MemberPayment> implements IMemberPaymentService {

    private final IMemberService memberService;

    public MemberPaymentServiceImpl(IMemberService memberService) {
        this.memberService = memberService;
    }

    @Override
    public void update(MemberPayment memberPayment, Integer id) {
        UpdateWrapper<MemberPayment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberPayment, updateWrapper);

        // 更新支付信息后  把会员昵称修改为银行卡姓名
        Member member = new Member();
        member.setNickname(memberPayment.getAccountName());
        member.setId(memberPayment.getMemberId());
        this.memberService.updateById(member);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberPayment> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public MemberPayment findByMemberId(Integer memberId) {
        QueryWrapper<MemberPayment> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        MemberPayment memberPayment = this.getOne(query);

        if(memberPayment == null) {
            memberPayment = new MemberPayment();
            memberPayment.setMemberId(memberId);
            this.save(memberPayment);
        }
        return memberPayment;
    }

}
