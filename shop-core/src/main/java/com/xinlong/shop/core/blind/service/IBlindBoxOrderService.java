package com.xinlong.shop.core.blind.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.BlindBoxOrder;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderQueryParam;
import com.xinlong.shop.core.blind.entity.dto.BlindBoxOrderDTO;
import com.xinlong.shop.core.order.entity.Order;
import com.xinlong.shop.core.order.entity.dto.CreateOrderDTO;

import java.util.List;

/**
 * <p>
 * 盲盒订单表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-06-13
 */
public interface IBlindBoxOrderService extends IService<BlindBoxOrder> {

    void update(BlindBoxOrder blindBoxOrder, Integer id);

    void delete(Integer id);

    BlindBoxOrder createOrder(BlindBox blindBox, String ruleId, Integer memberId);

    /**
     * 盲盒订单支付成功
     * @param orderSn
     * @param payOrderSn
     * @return
     */
    boolean paySuccess(String orderSn, String payOrderSn);

    IPage<BlindBoxOrderDTO> findByPage(IPage<BlindBoxOrderDTO> page, BlindBoxOrderQueryParam queryParam);

    List<BlindBoxOrderDTO> findByMemberId(Integer memberId);

    /**
     * 创建商城订单发货单
     * @param createOrderDTO 商城订单DTO
     * @param blindBoxOrderItemList 选中发货的盲盒订单项
     * @return
     */
    Order createShopOrder(CreateOrderDTO createOrderDTO, List<BlindBoxOrderItem> blindBoxOrderItemList);

}
