package com.xinlong.shop.core.address.mapper;

import com.xinlong.shop.core.address.entity.MemberAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 会员收货地址 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@Mapper
public interface MemberAddressMapper extends BaseMapper<MemberAddress> {

}
