package com.xinlong.shop.core.address.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地区表
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@TableName("sys_area")
public class SysArea implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地区ID
     */
    private Integer id;

    /**
     * 父级ID
     */
    private Integer parentId;

    /**
     * 地区深度
     */
    private Integer depth;

    /**
     * 地区名称
     */
    private String name;

    /**
     * 邮编
     */
    private String postalCode;

    /**
     * 地区排序
     */
    private Integer sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "SysArea{" +
            "id=" + id +
            ", parentId=" + parentId +
            ", depth=" + depth +
            ", name=" + name +
            ", postalCode=" + postalCode +
            ", sort=" + sort +
        "}";
    }
}
