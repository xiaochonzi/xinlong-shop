package com.xinlong.shop.core.blind.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderStatusEnum;
import com.xinlong.shop.core.blind.mapper.BlindBoxOrderItemMapper;
import com.xinlong.shop.core.blind.service.IBlindBoxOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 盲盒订单项 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-06-27
 */
@Service
public class BlindBoxOrderItemServiceImpl extends ServiceImpl<BlindBoxOrderItemMapper, BlindBoxOrderItem> implements IBlindBoxOrderItemService {

    @Override
    public void update(BlindBoxOrderItem blindBoxOrderItem, Integer id) {
        UpdateWrapper<BlindBoxOrderItem> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(blindBoxOrderItem, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<BlindBoxOrderItem> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public List<BlindBoxOrderItem> findByOrderId(Integer blindBoxOrderId) {
        QueryWrapper<BlindBoxOrderItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("blind_box_order_id", blindBoxOrderId);
        List<BlindBoxOrderItem> blindBoxOrderItems = this.list(queryWrapper);
        return blindBoxOrderItems;
    }

    @Override
    public List<BlindBoxOrderItem> findByMemberId(Integer memberId) {
        QueryWrapper<BlindBoxOrderItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id", memberId);
        List<BlindBoxOrderItem> blindBoxOrderItems = this.list(queryWrapper);
        return blindBoxOrderItems;
    }

    @Override
    public boolean pickUp(List<Integer> ids) {
        UpdateWrapper<BlindBoxOrderItem> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", BlindBoxOrderStatusEnum.PICK_UP.getCode());
        updateWrapper.in("id", ids);
        return this.update(updateWrapper);
    }

}
