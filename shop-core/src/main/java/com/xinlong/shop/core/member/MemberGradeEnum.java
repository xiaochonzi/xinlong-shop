package com.xinlong.shop.core.member;

/**
 * @Author Sylow
 * @Description 会员等级状态
 * @Date: Created in 9:48 2022/11/8
 */
public enum MemberGradeEnum {
    NORMAL(0, "正常"),
    DISABLE(1, "禁用"),
    DELETE(2, "删除");

    private Integer code;
    private String desc;

    MemberGradeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
