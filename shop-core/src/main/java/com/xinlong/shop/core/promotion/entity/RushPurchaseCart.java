package com.xinlong.shop.core.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 抢购购物车
 * </p>
 *
 * @author Sylow
 * @since 2023-05-05
 */
@TableName("s_rush_purchase_cart")
public class RushPurchaseCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 抢购活动明细id
     */
    private Integer rushPurchaseDetailId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品图片
     */
    private String goodsImg;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 创建时间戳
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getRushPurchaseDetailId() {
        return rushPurchaseDetailId;
    }

    public void setRushPurchaseDetailId(Integer rushPurchaseDetailId) {
        this.rushPurchaseDetailId = rushPurchaseDetailId;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "RushPurchaseCart{" +
            "id=" + id +
            ", rushPurchaseDetailId=" + rushPurchaseDetailId +
            ", goodsName=" + goodsName +
            ", goodsImg=" + goodsImg +
            ", price=" + price +
            ", memberId=" + memberId +
            ", createTime=" + createTime +
        "}";
    }
}
