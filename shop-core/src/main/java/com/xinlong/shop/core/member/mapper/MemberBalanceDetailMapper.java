package com.xinlong.shop.core.member.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.xinlong.shop.core.member.entity.MemberBalanceDetail;
import com.xinlong.shop.core.member.entity.vo.MemberBalanceDetailVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 用户余额明细 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@Mapper
public interface MemberBalanceDetailMapper extends BaseMapper<MemberBalanceDetail> {

    @Select("SELECT mbd.*,m.nickname,m.mobile FROM s_member_balance_detail mbd LEFT JOIN s_member m ON mbd.member_id = m.id ${ew.customSqlSegment}")
    IPage<MemberBalanceDetailVO> selectPageByVo(IPage<MemberBalanceDetailVO> page, @Param(Constants.WRAPPER) Wrapper queryWrapper);

}
