package com.xinlong.shop.core.blind.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;

import java.util.List;

/**
 * <p>
 * 盲盒订单项 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-06-27
 */
public interface IBlindBoxOrderItemService extends IService<BlindBoxOrderItem> {

    void update(BlindBoxOrderItem blindBoxOrderItem, Integer id);

    void delete(Integer id);

    /**
     * 根据盲盒订单id 获取订单项
     * @param blindBoxOrderId
     * @return
     */
    List<BlindBoxOrderItem> findByOrderId(Integer blindBoxOrderId);

    List<BlindBoxOrderItem> findByMemberId(Integer memberId);

    /**
     * 盲盒订单项批量提货
     * @param ids
     * @return
     */
    boolean pickUp(List<Integer> ids);
}
