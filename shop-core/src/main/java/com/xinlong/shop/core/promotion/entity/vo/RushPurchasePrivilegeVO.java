package com.xinlong.shop.core.promotion.entity.vo;

/**
 * @Author Sylow
 * @Description 抢购特权VO
 * @Date: Created in 16:14 2023/5/14
 */
public class RushPurchasePrivilegeVO {

    /**
     * 提前入场时间
     */
    private Integer earlyEntryTime = 0;

    /**
     * 是否包含购物车 = 0等于没有  1=有
     */
    private Integer isHaveCart = 0;

    public Integer getEarlyEntryTime() {
        return earlyEntryTime;
    }

    public void setEarlyEntryTime(Integer earlyEntryTime) {
        this.earlyEntryTime = earlyEntryTime;
    }

    public Integer getIsHaveCart() {
        return isHaveCart;
    }

    public void setIsHaveCart(Integer isHaveCart) {
        this.isHaveCart = isHaveCart;
    }
}
