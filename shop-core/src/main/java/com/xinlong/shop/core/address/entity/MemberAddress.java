package com.xinlong.shop.core.address.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 会员收货地址
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@TableName("s_member_address")
public class MemberAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 收货地区ID
     */
    private Integer areaId;

    /**
     * 收货地区名称
     */
    @NotBlank(message = "收货地区名称不能为空")
    private String areaName;

    /**
     * 收货详细地址
     */
    @NotBlank(message = "详细地址不能为空")
    private String address;

    /**
     * 收货人姓名
     */
    @NotBlank(message = "收货人不能为空")
    private String name;

    /**
     * 收货电话
     */
    @NotBlank(message = "收货电话不能为空")
    private String mobile;

    /**
     * 是否默认 1=默认 0=不默认
     */
    private Integer isDef;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
    public String getAddress() {
        return address;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public Integer getIsDef() {
        return isDef;
    }

    public void setIsDef(Integer isDef) {
        this.isDef = isDef;
    }

    @Override
    public String toString() {
        return "MemberAddress{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", areaId=" + areaId +
            ", areaName=" + areaName +
            ", address=" + address +
            ", name=" + name +
            ", mobile=" + mobile +
            ", isDef=" + isDef +
        "}";
    }
}
