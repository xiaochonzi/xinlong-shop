package com.xinlong.shop.core.promotion.mapstruct;

import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 20:56 2023/2/19
 */
@Mapper(componentModel = "spring")
public interface RushPurchaseStruct {
    RushPurchaseStruct INSTANCE = Mappers.getMapper(RushPurchaseStruct.class);

    RushPurchase toRushPurchase(RushPurchaseDTO rushPurchaseDTO);
}
