package com.xinlong.shop.core.member.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinlong.shop.core.member.entity.MemberWithdrawal;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 用户提现记录DTO
 * </p>
 *
 * @author Sylow
 * @since 2023-04-11
 */
@TableName(autoResultMap = true)
public class MemberWithdrawalDTO extends MemberWithdrawal implements Serializable {

    private String memberName;

    private String nickname;

    private String mobile;

    private BigDecimal balance;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
