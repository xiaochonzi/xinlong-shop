package com.xinlong.shop.core.promotion.service;

import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 抢购活动表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
public interface IRushPurchaseService extends IService<RushPurchase> {

    void update(RushPurchase rushPurchase, Integer id);

    void delete(Integer id);

    /**
     * 更新抢购活动状态
     * @param id
     * @param status 状态
     */
    void updateStatus(Integer id, Integer status);

    /**
     * 获取指定时间段内的开启的活动
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<RushPurchase> findByTime(Long startTime, Long endTime);

    /**
     * 通过上一场次id 得到新生成的活动场次信息
     * @param id
     * @return
     */
    RushPurchase findByLastId(Integer id);
}
