package com.xinlong.shop.core.goods.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.goods.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品品牌 服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-12-07
 */
public interface IBrandService extends IService<Brand> {

    void update(Brand brand, Integer id);

    void delete(Integer id);
}
