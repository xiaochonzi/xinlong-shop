package com.xinlong.shop.core.member.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.member.MemberWithdrawalQueryParam;
import com.xinlong.shop.core.member.MemberWithdrawalStatus;
import com.xinlong.shop.core.member.entity.MemberWithdrawal;
import com.xinlong.shop.core.member.entity.dto.MemberWithdrawalDTO;
import com.xinlong.shop.core.member.mapper.MemberWithdrawalDTOMapper;
import com.xinlong.shop.core.member.mapper.MemberWithdrawalMapper;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.member.service.IMemberWithdrawalService;
import com.xinlong.shop.framework.exception.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 用户提现记录 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@Service
public class MemberWithdrawalServiceImpl extends ServiceImpl<MemberWithdrawalMapper, MemberWithdrawal> implements IMemberWithdrawalService {

    private final IMemberService memberService;

    private final MemberWithdrawalDTOMapper memberWithdrawalDTOMapper;

    public MemberWithdrawalServiceImpl(IMemberService memberService, MemberWithdrawalDTOMapper memberWithdrawalDTOMapper) {
        this.memberService = memberService;
        this.memberWithdrawalDTOMapper = memberWithdrawalDTOMapper;
    }

    @Override
    public void update(MemberWithdrawal memberWithdrawal, Integer id) {
        UpdateWrapper<MemberWithdrawal> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberWithdrawal, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberWithdrawal> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public Page<MemberWithdrawal> page(Page<MemberWithdrawal> page, Integer memberId) {
        QueryWrapper<MemberWithdrawal> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        page.addOrder(new OrderItem("create_time",false));
        return this.page(page, query);
    }

    @Override
    public Page<MemberWithdrawalDTO> page(Page<MemberWithdrawalDTO> page, MemberWithdrawalQueryParam memberWithdrawalQueryParam) {
        // 这里配合xml中sql  create_time有两个  里面join表了 所以要指定表
        page.addOrder(new OrderItem("md.create_time",false));
        return this.memberWithdrawalDTOMapper.findByPage(page, memberWithdrawalQueryParam);
    }

    @Override
    public BigDecimal findTotalByMemberId(Integer memberId) {
        QueryWrapper<MemberWithdrawal> query = new QueryWrapper<>();
        query.select("SUM(price) as price");
        query.eq("member_id", memberId);
        query.eq("status", MemberWithdrawalStatus.PAY.getCode());
        // MemberWithdrawal 没有专门的dto
        MemberWithdrawal memberWithdrawal = this.getOne(query);
        if (memberWithdrawal == null) {
            return BigDecimal.valueOf(0);
        } else {
            return memberWithdrawal.getPrice();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(MemberWithdrawal memberWithdrawal) {
        boolean result = this.save(memberWithdrawal);
        if (result == true) {
            this.memberService.subBalance(memberWithdrawal.getMemberId(), memberWithdrawal.getPrice());
        } else {
            throw new ServiceException("保存失败");
        }
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        UpdateWrapper<MemberWithdrawal> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.set("update_time", DateUtil.currentSeconds());
        updateWrapper.eq("id", id);
        this.update(updateWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void refuse(Integer id) {
        this.updateStatus(id, MemberWithdrawalStatus.REFUSE.getCode());
        MemberWithdrawal memberWithdrawal = this.getById(id);

        // 恢复用户余额
        this.memberService.addBalance(memberWithdrawal.getMemberId(), memberWithdrawal.getPrice());
    }

}
