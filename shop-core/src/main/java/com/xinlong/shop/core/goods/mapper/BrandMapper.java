package com.xinlong.shop.core.goods.mapper;

import com.xinlong.shop.core.goods.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 商品品牌 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-12-07
 */
@Mapper
public interface BrandMapper extends BaseMapper<Brand> {

}
