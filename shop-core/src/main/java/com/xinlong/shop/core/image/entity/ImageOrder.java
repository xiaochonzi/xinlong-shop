package com.xinlong.shop.core.image.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 图片下载订单
 * </p>
 *
 * @author Sylow
 * @since 2023-08-04
 */
@TableName("s_image_order")
public class ImageOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 金额
     */
    private BigDecimal price;

    /**
     * 图片地址
     */
    private String imageUrl;

    /**
     * 状态0未支付1已支付
     */
    private Integer status;

    /**
     * 支付单号
     */
    private String payOrderSn;

    /**
     * 创建时间戳
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPayOrderSn() {
        return payOrderSn;
    }

    public void setPayOrderSn(String payOrderSn) {
        this.payOrderSn = payOrderSn;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ImageOrder{" +
            "id=" + id +
            ", orderSn=" + orderSn +
            ", memberId=" + memberId +
            ", price=" + price +
            ", imageUrl=" + imageUrl +
            ", status=" + status +
            ", payOrderSn=" + payOrderSn +
            ", createTime=" + createTime +
        "}";
    }
}
