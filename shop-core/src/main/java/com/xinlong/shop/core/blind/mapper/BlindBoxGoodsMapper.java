package com.xinlong.shop.core.blind.mapper;

import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 盲盒关联商品表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@Mapper
public interface BlindBoxGoodsMapper extends BaseMapper<BlindBoxGoods> {

}
