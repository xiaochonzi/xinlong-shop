package com.xinlong.shop.core.site.entity.dto;

import com.xinlong.shop.core.site.entity.Adv;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 14:12 2023/5/29
 */
@ApiModel(value = "广告列表")
public class AdvListDTO extends Adv {

    @ApiModelProperty(value = "广告位名称")
    private String promotionName;

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }
}
