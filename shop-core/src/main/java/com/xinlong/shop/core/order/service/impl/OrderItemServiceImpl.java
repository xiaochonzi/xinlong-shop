package com.xinlong.shop.core.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.order.entity.OrderItem;
import com.xinlong.shop.core.order.mapper.OrderItemMapper;
import com.xinlong.shop.core.order.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 订单子项表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

    @Override
    public void update(OrderItem orderItem, Integer id) {
        UpdateWrapper<OrderItem> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(orderItem, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<OrderItem> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public List<OrderItem> listByOrderId(Integer orderId) {
        QueryWrapper<OrderItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        return this.list(queryWrapper);

    }

    @Override
    public boolean updateStatus(Integer orderId, Integer status) {
        UpdateWrapper<OrderItem> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id", orderId);
        updateWrapper.set("status", status);
        return this.update(updateWrapper);
    }

}
