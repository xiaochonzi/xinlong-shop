package com.xinlong.shop.core.goods.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.goods.entity.Brand;
import com.xinlong.shop.core.goods.mapper.BrandMapper;
import com.xinlong.shop.core.goods.service.IBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 商品品牌 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2022-12-07
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

    @Override
    public void update(Brand brand, Integer id) {
        UpdateWrapper<Brand> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(brand, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<Brand> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

}
