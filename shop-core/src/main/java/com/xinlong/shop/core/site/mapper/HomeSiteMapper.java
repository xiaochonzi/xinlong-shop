package com.xinlong.shop.core.site.mapper;

import com.xinlong.shop.core.site.entity.HomeSite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 首页位置 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@Mapper
public interface HomeSiteMapper extends BaseMapper<HomeSite> {

}
