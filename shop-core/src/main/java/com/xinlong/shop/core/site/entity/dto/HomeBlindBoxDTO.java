package com.xinlong.shop.core.site.entity.dto;

import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "首页显示的盲盒DTO")
public class HomeBlindBoxDTO extends BlindBox {
    @ApiModelProperty(value = "位置编码")
    private String siteCode;

    @ApiModelProperty(value = "位置名称")
    private String siteName;

    @ApiModelProperty(value = "位置排序")
    private Integer sort;

    private List<BlindBoxGoods> blindBoxGoodsList;


    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<BlindBoxGoods> getBlindBoxGoodsList() {
        return blindBoxGoodsList;
    }

    public void setBlindBoxGoodsList(List<BlindBoxGoods> blindBoxGoodsList) {
        this.blindBoxGoodsList = blindBoxGoodsList;
    }
}
