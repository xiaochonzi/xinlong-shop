package com.xinlong.shop.core.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.xinlong.shop.core.order.OrderQueryParam;
import com.xinlong.shop.core.order.entity.Order;
import com.xinlong.shop.core.order.entity.dto.CreateOrderDTO;
import com.xinlong.shop.core.order.entity.vo.OrderVO;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
public interface IOrderService extends IService<Order> {

    void update(Order order, Integer id);

    void delete(Integer id);

    Order findByOrderSn(String orderSn);

    IPage<Order> selectPage(IPage<Order> page, OrderQueryParam orderQueryParam);

    Order createOrder(CreateOrderDTO createOrderDTO);

    /**
     * 订单支付成功
     * @param orderSn
     * @param payOrderSn
     */
    void paySuccess(String orderSn, String payOrderSn);

    IPage<OrderVO> selectPageOrderVo(IPage<OrderVO> page, OrderQueryParam orderQueryParam);

    /**
     * 订单发货
     * @param orderId 订单ID
     * @param logisticsId   物流公司id
     * @param logisticsName 物流公司名称
     * @param logisticsSn  物流单号
     */
    void shipment(Integer orderId, Integer logisticsId, String logisticsName, String logisticsSn);

    /**
     * 修改订单状态
     * @param orderId
     * @param status
     */
    void updateOrderStatus(Integer orderId, Integer status);

    /**
     * 创建盲盒发货订单（提货使用）
     * @param createOrderDTO
     * @param blindBoxOrderItemList
     * @return
     */
    Order createBlindOrder(CreateOrderDTO createOrderDTO, List<BlindBoxOrderItem> blindBoxOrderItemList);
}
