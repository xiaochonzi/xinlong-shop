package com.xinlong.shop.core.goods.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinlong.shop.core.goods.entity.Goods;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 15:48 2022/11/15
 */
@TableName(autoResultMap = true)
public class GoodsDTO extends Goods {

    /**
     * 商品分类名称
     */
    private String classifyName;

    /**
     * 商品分类名称
     */
    private String brandName;

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
