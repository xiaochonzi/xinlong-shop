package com.xinlong.shop.core.member.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.member.entity.MemberActivityIncome;
import com.xinlong.shop.core.promotion.mapper.ActivityIncomeMapper;
import com.xinlong.shop.core.member.service.IMemberActivityIncomeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.math.BigDecimal;

/**
 * <p>
 * 会员收益表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-30
 */
@Service
public class MemberActivityIncomeServiceImpl extends ServiceImpl<ActivityIncomeMapper, MemberActivityIncome> implements IMemberActivityIncomeService {

    @Override
    public void update(MemberActivityIncome memberActivityIncome, Integer id) {
        UpdateWrapper<MemberActivityIncome> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberActivityIncome, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberActivityIncome> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void addActivityIncome(Integer memberId, BigDecimal buyPrice, BigDecimal sellPrice, BigDecimal servicePrice, BigDecimal profit, Integer rushPurchaseDetailId, String activityId) {

        // 先检查是否已经有这条数据了
        QueryWrapper<MemberActivityIncome> queryWrapper = new QueryWrapper();
        queryWrapper.eq("rush_purchase_detail_id", rushPurchaseDetailId);
        MemberActivityIncome memberActivityIncome = this.getOne(queryWrapper);

        if (memberActivityIncome == null) {
            memberActivityIncome = new MemberActivityIncome(memberId, buyPrice, sellPrice, servicePrice, profit, rushPurchaseDetailId, activityId);
            memberActivityIncome.setCreateTime(DateUtil.currentSeconds());

            this.save(memberActivityIncome);
        }
    }

    @Override
    public IPage<MemberActivityIncome> page(IPage<MemberActivityIncome> page, Integer memberId) {
        QueryWrapper<MemberActivityIncome> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        query.orderByDesc("create_time");
        return this.page(page, query);
    }

    @Override
    public BigDecimal getTotal(Integer memberId) {
        return this.baseMapper.getTotal(memberId);
    }

    @Override
    public BigDecimal getTotalByTime(Integer memberId, Long startTime, Long endTime) {

        return this.baseMapper.getTotalByTime(memberId, startTime, endTime);
    }

    @Override
    public MemberActivityIncome findByRushPurchaseDetailId(Integer rushPurchaseDetailId) {
        QueryWrapper<MemberActivityIncome> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("rush_purchase_detail_id", rushPurchaseDetailId);
        return this.getOne(queryWrapper);
    }

}
