package com.xinlong.shop.core.site.service;

import com.xinlong.shop.core.site.entity.HomeSiteBlind;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.site.entity.dto.HomeBlindBoxDTO;
import com.xinlong.shop.core.site.entity.vo.HomeSiteBlindVO;

import java.util.List;

/**
 * <p>
 * 首页显示的盲盒商品 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
public interface IHomeSiteBlindService extends IService<HomeSiteBlind> {

    void update(HomeSiteBlind homeSiteBlind, Integer id);

    void delete(Integer id);

    /**
     * 批量保存
     * @param homeSiteBlindVO
     */
    void batchSaveBlindBox(HomeSiteBlindVO homeSiteBlindVO);

    List<HomeBlindBoxDTO> findBlindBoxList(String siteCode);

    void clearBlindBox(String siteCode);

}
