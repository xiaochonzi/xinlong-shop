package com.xinlong.shop.core.member.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.member.entity.MemberPointsDetail;
import com.xinlong.shop.core.member.mapper.MemberPointsDetailMapper;
import com.xinlong.shop.core.member.service.IMemberPointsDetailService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 用户积分明细 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-07-22
 */
@Service
public class MemberPointsDetailServiceImpl extends ServiceImpl<MemberPointsDetailMapper, MemberPointsDetail> implements IMemberPointsDetailService {

    @Override
    public void update(MemberPointsDetail memberPointsDetail, Integer id) {
        UpdateWrapper<MemberPointsDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberPointsDetail, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberPointsDetail> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void add(Integer memberId, String source, Integer type, BigDecimal points, String memo) {
        MemberPointsDetail memberPointsDetail = new MemberPointsDetail();
        memberPointsDetail.setMemberId(memberId);
        memberPointsDetail.setSource(source);
        memberPointsDetail.setType(type);
        memberPointsDetail.setPoints(points);
        memberPointsDetail.setMemo(memo);
        memberPointsDetail.setCreateTime(DateUtil.currentSeconds());
        this.save(memberPointsDetail);
    }

}
