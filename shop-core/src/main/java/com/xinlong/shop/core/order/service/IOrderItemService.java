package com.xinlong.shop.core.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.order.entity.OrderItem;

import java.util.List;

/**
 * <p>
 * 订单子项表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
public interface IOrderItemService extends IService<OrderItem> {

    void update(OrderItem orderItem, Integer id);

    void delete(Integer id);

    List<OrderItem> listByOrderId(Integer orderId);

    /**
     * 修改订单子项状态
     * @param orderId
     * @param status
     * @return
     */
    boolean updateStatus(Integer orderId, Integer status);
}
