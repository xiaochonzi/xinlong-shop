package com.xinlong.shop.core.site.service;

import com.xinlong.shop.core.site.entity.AdvPromotion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 广告位 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
public interface IAdvPromotionService extends IService<AdvPromotion> {

    void update(AdvPromotion advPromotion, Integer id);

    void delete(Integer id);

    AdvPromotion getByCode(String code);
}
