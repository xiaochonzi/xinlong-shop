package com.xinlong.shop.core.goods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@TableName("s_goods_classify")
public class GoodsClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分类名称
     */
    private String classifyName;

    /**
     * 分类图片
     */
    private String classifyImg;

    /**
     * 是否展示,0=展示,1=隐藏
     */
    private Integer isShow;

    /**
     * 父id
     */
    private Integer pid;

    /**
     * 分类路径0|1|2,便于查询
     */
    private String classifyPath;

    /**
     * 是否禁用,0=正常,1=删除
     */
    private Integer disable;

    /**
     * 排序
     */
    private Integer sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
    public String getClassifyImg() {
        return classifyImg;
    }

    public void setClassifyImg(String classifyImg) {
        this.classifyImg = classifyImg;
    }
    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }
    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
    public String getClassifyPath() {
        return classifyPath;
    }

    public void setClassifyPath(String classifyPath) {
        this.classifyPath = classifyPath;
    }
    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "GoodsClassify{" +
            "id=" + id +
            ", classifyName=" + classifyName +
            ", classifyImg=" + classifyImg +
            ", isShow=" + isShow +
            ", pid=" + pid +
            ", classifyPath=" + classifyPath +
            ", disable=" + disable +
            ", sort=" + sort +
        "}";
    }
}
