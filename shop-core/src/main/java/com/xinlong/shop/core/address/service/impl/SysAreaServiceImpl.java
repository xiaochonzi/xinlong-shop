package com.xinlong.shop.core.address.service.impl;

import com.xinlong.shop.core.address.entity.SysArea;
import com.xinlong.shop.core.address.mapper.SysAreaMapper;
import com.xinlong.shop.core.address.service.ISysAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 地区表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysArea> implements ISysAreaService {

    @Override
    public void update(SysArea sysArea, Integer id) {
        UpdateWrapper<SysArea> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(sysArea, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<SysArea> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

}
