package com.xinlong.shop.core.blind.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author Sylow
 * @Description 盲盒订单查询参数
 * @Date: Created in 10:33 2023/6/27
 */
@ApiModel(description = "盲盒订单查询参数")
public class BlindBoxOrderQueryParam {

    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "会员姓名（昵称）")
    private String nickname;

    @ApiModelProperty(value = "会员手机号")
    private String memberMobile;

    @ApiModelProperty(value = "订单状态")
    private Integer status;

    @ApiModelProperty(value = "盲盒名称")
    private String blindBoxName;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBlindBoxName() {
        return blindBoxName;
    }

    public void setBlindBoxName(String blindBoxName) {
        this.blindBoxName = blindBoxName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
