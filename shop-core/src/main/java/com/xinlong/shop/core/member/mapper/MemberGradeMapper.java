package com.xinlong.shop.core.member.mapper;

import com.xinlong.shop.core.member.entity.MemberGrade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员等级 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
public interface MemberGradeMapper extends BaseMapper<MemberGrade> {

}
