package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.entity.MemberSign;

/**
 * <p>
 * 会员签名记录表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
public interface IMemberSignService extends IService<MemberSign> {

    void update(MemberSign memberSign, Integer id);

    void delete(Integer id);

    IPage page(IPage page, Integer status);

    void pass(Integer id);

    MemberSign findByMemberId(Integer memberId);

    void addSign(Integer memberId, String signImg);
}
