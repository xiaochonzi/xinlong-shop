package com.xinlong.shop.core.promotion.entity.dto;

import javax.validation.constraints.NotBlank;

/**
 * @Author Sylow
 * @Description 后台管理维护数据使用
 * @Date: Created in 20:52 2023/2/19
 */
public class RushPurchaseDTO {
    /**
     * 主健
     */
    private Integer id;

    /**
     * 活动名称
     */
    @NotBlank(message = "活动名称不能为空")
    private String activityName;

    @NotBlank(message = "活动ID不能为空")
    private String activityId;

    /**
     * 活动时间
     */
    private Long[] activityTime;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 活动状态：未开始、进行中、已结束、关闭
     */
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Long[] getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(Long[] activityTime) {
        this.activityTime = activityTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RushPurchase{" +
                "id=" + id +
                ", activityName=" + activityName +
                ", activityId=" + activityId +
                ", activityTime=" + activityTime +
                ", createTime=" + createTime +
                ", status=" + status +
                "}";
    }
}
