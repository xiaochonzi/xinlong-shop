package com.xinlong.shop.core.logistics.entity;

/**
 * 物流插件枚举类
 */
public enum LogisticsEnum {
    // 快递100，快递鸟
    KUAIDI100, KDNIAO;
}
