package com.xinlong.shop.core.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.member.entity.dto.MemberListDTO;
import com.xinlong.shop.core.member.entity.dto.MemberSearchDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员列表DTO Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@Mapper
public interface MemberListDTOMapper extends BaseMapper<MemberListDTO> {

    IPage<MemberListDTO> findMemberPage(IPage<MemberListDTO> page, @Param("memberSearch") MemberSearchDTO memberSearch);

    MemberListDTO findMemberById(@Param("id") Integer id);

}
