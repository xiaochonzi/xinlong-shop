package com.xinlong.shop.core.member.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinlong.shop.framework.core.entity.Member;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 会员列表dto 一般是后台管理用
 * @Date: Created in 21:06 2023/04/13
 */
@ApiModel(value = "会员列表DTO")
@TableName(autoResultMap = true)
public class MemberListDTO extends Member implements Serializable {

    @ApiModelProperty(value = "邀请人会员名")
    private String inviteMemberName;

    @ApiModelProperty(value = "邀请人会员昵称")
    private String inviteMemberNickname;

    @ApiModelProperty(value = "邀请人会员手机号")
    private String inviteMemberMobile;

    @ApiModelProperty(value = "会员等级名称")
    private String gradeName;

    public String getInviteMemberName() {
        return inviteMemberName;
    }

    public void setInviteMemberName(String inviteMemberName) {
        this.inviteMemberName = inviteMemberName;
    }

    public String getInviteMemberNickname() {
        return inviteMemberNickname;
    }

    public void setInviteMemberNickname(String inviteMemberNickname) {
        this.inviteMemberNickname = inviteMemberNickname;
    }

    public String getInviteMemberMobile() {
        return inviteMemberMobile;
    }

    public void setInviteMemberMobile(String inviteMemberMobile) {
        this.inviteMemberMobile = inviteMemberMobile;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
