package com.xinlong.shop.core.promotion.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 11:39 2023/5/15
 */
@TableName(autoResultMap = true)
public class RushPurchaseCartDTO extends RushPurchaseCart {

    /**
     * 买家会员昵称（姓名）
     */
    private String nickname;

    private String mobile;

    private String sellerName;

    private String sellerMobile;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerMobile() {
        return sellerMobile;
    }

    public void setSellerMobile(String sellerMobile) {
        this.sellerMobile = sellerMobile;
    }
}
