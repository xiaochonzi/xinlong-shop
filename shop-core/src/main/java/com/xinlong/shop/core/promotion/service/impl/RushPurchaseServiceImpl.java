package com.xinlong.shop.core.promotion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.xinlong.shop.core.promotion.mapper.RushPurchaseMapper;
import com.xinlong.shop.core.promotion.service.IRushPurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 抢购活动表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
@Service
public class RushPurchaseServiceImpl extends ServiceImpl<RushPurchaseMapper, RushPurchase> implements IRushPurchaseService {

    @Override
    public void update(RushPurchase rushPurchase, Integer id) {
        UpdateWrapper<RushPurchase> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(rushPurchase, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<RushPurchase> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        UpdateWrapper<RushPurchase> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        RushPurchase rushPurchase = new RushPurchase();
        rushPurchase.setStatus(status);
        this.update(rushPurchase, updateWrapper);
    }

    @Override
    public List<RushPurchase> findByTime(Long startTime, Long endTime) {
        QueryWrapper<RushPurchase> query = new QueryWrapper();
        // 开始时间在条件结束时间范围内，小于条件结束时间
        query.le("start_time", endTime);
        // 结束时间 在条件开始时间之后的
        query.gt("end_time", startTime);
        // 正常开启活动
        query.eq("status", 0);
        query.orderByAsc("start_time");

        return this.list(query);
    }

    @Override
    public RushPurchase findByLastId(Integer id) {
        QueryWrapper<RushPurchase> query = new QueryWrapper();
        // 正常开启活动
        query.eq("last_id", id);
        return this.getOne(query);
    }

}
