package com.xinlong.shop.core.goods.entity.vo;

import com.xinlong.shop.core.goods.entity.GoodsClassify;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 管理端VO对象
 * @Date: Created in 11:51 2022/11/8
 */
public class GoodsClassifyVO extends GoodsClassify implements Serializable{

    /**
     * 配合前端naiceui的树形结构
     */
    private boolean isLeaf = false;

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }
}
