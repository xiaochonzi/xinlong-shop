<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>信隆代码生成器</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/js/css/layui.css" rel="stylesheet">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-inline">
                <label class="layui-form-label">表名</label>
                <div class="layui-input-inline">
                    <input id="tableName" name="tableName" class="layui-input" type="text" placeholder="请输入表名"/>
                </div>
            </div>
            <div class="layui-inline">
                <button type="button" id="searchBtn" class="layui-btn layui-btn-normal icon-btn layui-btn-sm">
                    <i class="layui-icon">&#xe615;</i> 搜索
                </button>
            </div>

        </div>
        <div class="layui-inline">
            <button type="button" id="generateCode" class="layui-btn layui-btn-normal icon-btn layui-btn-sm">
                <i class="layui-icon">&#xe64e;</i> 生成代码
            </button>
        </div>
    <table class="layui-hide" id="tables"></table>
    </div>


</div>
<form id="generateCodeLayer" class="layui-form" lay-filter="generateCode" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">作者</label>
        <div class="layui-input-block">
            <input type="text" name="author" required  value="Sylow" lay-verify="required" placeholder="请输入作者" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">包名</label>
        <div class="layui-input-block">
            <input type="text" name="packageConfig" value="com.xinlong.shop" required lay-verify="required" placeholder="请输入包名" autocomplete="off" class="layui-input">
        </div>
<#--        <div class="layui-form-mid layui-word-aux">辅助文字</div>-->
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">模块名</label>
        <div class="layui-input-block">
            <input type="text" name="module" required value="core"  lay-verify="required" placeholder="请输入模块名" autocomplete="off" class="layui-input">
        </div>
    </div>
</form>

</body>
<script src="/js/layui.js"></script>


<script>
    layui.use(['jquery','table','layer','form'], function(){
        var $ = layui.jquery, table = layui.table, layer = layui.layer, form = layui.form;
        table.render({
            elem: '#tables'
            ,url: '/sys/tables'
            ,cellMinWidth: 180 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {type:'numbers', width:80, title: '序号',fixed: 'left'}
                ,{type:'checkbox', width:50,fixed: 'left'}
                ,{field:'tableName', width:180, title: '表名', sort: true}
                ,{field:'tableComment', width:180, title: '描述'}
                ,{field:'tableRows', title: '数据行数', width: 180}
                ,{field:'createTime', title: '创建时间', width: 180, sort: true}
                ,{field:'updateTime', title: '更新时间', width: 180}
            ]]
            ,parseData:function(res){
                console.log(res)
                return {
                    "code" : 0,
                    "msg" : "",
                    "data" : res.result
                }
            }
        });

        $('#searchBtn').on('click',function () {
            var tableName = $("#tableName").val();
            table.reload("tables",{
                where: {"tableName" : tableName}
            });
        });

        $('#generateCode').on('click',function(){
            var checkData = table.checkStatus('tables').data;
            console.log(checkData);
            if(checkData.length < 1) {
                layer.msg('至少选择一张表');
                return;
            }

            layer.open({
                area: ['500px', '300px'],
                type: 1,
                title: '代码详情',
                content: $('#generateCodeLayer'),
                btn: ['生成', '取消'],
                yes: function (index, layero) {

                    form.submit('generateCode', function(e){
                        var data = form.val('generateCode');
                        var tables = [];
                        $.each(checkData,function(k,v){
                            tables.push(v.tableName);
                        })
                        data.tables = tables;

                        console.log(data);
                        $.ajax({
                            url : '/sys/base-gen',
                            contentType: 'application/json',
                            type: 'post',
                            data: JSON.stringify(data),
                            success: function(res) {
                                if(res.code == "200") {
                                    layer.msg(res.message);
                                    layer.close(index);
                                }
                            }
                        });
                    });
                },
                // no: function (index, layero) {
                //
                // },
                cancel: function (index) {

                }
            });
        });
    });
</script>
<style>
    .layui-form-label {
        text-align: center;
    }
    #generateCodeLayer {
        display: none;
        padding: 20px;
    }
</style>
</html>