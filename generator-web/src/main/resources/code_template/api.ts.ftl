import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}
<#assign apiBase = "" />
<#if package.ModuleName?? && package.ModuleName != "">
    <#assign apiBase = '/' + package.ModuleName />
</#if>
<#assign apiBase = apiBase + '/' />
<#if controllerMappingHyphenStyle>
    <#assign apiBase = apiBase + controllerMappingHyphen />
<#else>
    <#assign apiBase = apiBase + table.entityPath />
</#if>

/**
 * @description: ${table.comment!}列表
 */
export function get${entity}Page(params?) {
  return http.request({
    url: '${apiBase}/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存${table.comment!}
 * @param params
 */
export function save${entity}(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `${apiBase}`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑${table.comment!}
 * @param params
 */
export function update${entity}(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `${apiBase}/${r'$'}{params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除${table.comment!}
 * @param params
 */
export function delete${entity}(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `${apiBase}/${r'$'}{params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
