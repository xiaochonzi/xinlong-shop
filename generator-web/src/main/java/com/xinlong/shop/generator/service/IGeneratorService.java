package com.xinlong.shop.generator.service;

import com.xinlong.shop.generator.entity.TableInfo;
import com.xinlong.shop.generator.model.GeneratorVO;

import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:26 2022/6/12
 */
public interface IGeneratorService {


    Map findFieldsByTableName(String[] tableNames);

    /**
     * 通过表名搜索相关表信息
     * @param tableName
     * @return
     */
    List<TableInfo> findTablesByTableName(String tableName);

    /**
     * 执行代码生成
     * @param generatorVO
     * @return
     */
    String execute(GeneratorVO generatorVO);
}
