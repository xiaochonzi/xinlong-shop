package com.xinlong.shop.generator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:18 2022/6/14
 */
@Controller
public class GeneratorViewController {

    @GetMapping("/gen")
    public String index(){
        return "index";
    }
}
