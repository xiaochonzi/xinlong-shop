package com.xinlong.shop.generator.model;

/**
 * @Author Sylow
 * @Description 扩展模板
 * @Date: Created in 17:23 2022/6/12
 */
public class ExtTemplates {


    private String path;

    private String templateFilePath;

    private String outFileName;

    private String fileType;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTemplateFilePath() {
        return templateFilePath;
    }

    public void setTemplateFilePath(String templateFilePath) {
        this.templateFilePath = templateFilePath;
    }

    public String getOutFileName() {
        return outFileName;
    }

    public void setOutFileName(String outFileName) {
        this.outFileName = outFileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
