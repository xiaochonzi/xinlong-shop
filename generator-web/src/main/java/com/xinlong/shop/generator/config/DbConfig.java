package com.xinlong.shop.generator.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description 数据库配置
 * @Date: Created in 17:18 2022/6/12
 */
@Component
@ConfigurationProperties(prefix = "spring.datasource")
public class DbConfig {

    private String userName;

    private String password;

    private String url;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
