package com.xinlong.shop.api.goods;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.goods.entity.Brand;
import com.xinlong.shop.core.goods.service.IBrandService;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 商品品牌 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-12-07
 */
@RestController
@RequestMapping("/goods/brand")
public class BrandController {

    private final IBrandService brandService;

    public BrandController(IBrandService brandService) {
        this.brandService = brandService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<Brand> page = new Page<>(current, size);
        page.addOrder(new OrderItem("sort", true));
        page = this.brandService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Brand brand ){
        this.brandService.save(brand);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody Brand brand, @PathVariable("id") Integer id){
        this.brandService.update(brand, id);
        return R.success("操作成功",brand);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.brandService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(){
        List<Brand> list = this.brandService.list();
        return R.success(list);
    }

}
