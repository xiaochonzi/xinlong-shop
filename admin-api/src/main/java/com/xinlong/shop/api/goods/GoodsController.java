package com.xinlong.shop.api.goods;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.GoodsTypeEnum;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.goods.entity.Goods;
import com.xinlong.shop.core.goods.service.IGoodsService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@RestController
@RequestMapping("/goods/goods")
public class GoodsController {

    private final IGoodsService goodsService;

    public GoodsController(IGoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, GoodsQueryParam goodsQueryParam){
        if(current == null) {
            current = 1;
        }
        if(size == null) {
            size = 10;
        }
        goodsQueryParam.setSortName("create_time");
        goodsQueryParam.setSortType("desc");
        IPage<GoodsDTO> page = new Page<>(current, size);
        page = this.goodsService.page(page, goodsQueryParam);
        System.out.println(page.getPages());
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Goods goods ){
        // 创建时间为当前时间
        goods.setCreateTime(DateUtil.currentSeconds());
        this.goodsService.save(goods);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public R get(@PathVariable("id") Integer id){
        Goods goods = this.goodsService.getById(id);
        return R.success("操作成功",goods);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@RequestBody Goods goods, @PathVariable("id") Integer id){
        this.goodsService.update(goods, id);
        return R.success("操作成功",goods);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.goodsService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/status", method = RequestMethod.PUT)
    public R show(Integer id, Integer status) {
        if (status == null) {
            status = 0;
        }
        this.goodsService.updateStatus(id, status);
        return R.success("操作成功");
    }

    /**
     * 获取所有商品，给商品选择器使用，后期更新成为分页的
     * @return
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public R allGoods(String goodsName, String goodsType){
        if (goodsName == null) {
            goodsName = "";
        }
        Integer goodsTypeCode = null;
        if (goodsType != null && !"".equals(goodsType)) {
            goodsTypeCode = GoodsTypeEnum.valueOf(goodsType.toUpperCase()).getCode();
        }
        List<Goods> goods = goodsService.findAllGoods(goodsName, goodsTypeCode);
        return R.success("操作成功",goods);
    }

}
