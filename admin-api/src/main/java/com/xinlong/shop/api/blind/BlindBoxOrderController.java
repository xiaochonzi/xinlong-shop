package com.xinlong.shop.api.blind;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderQueryParam;
import com.xinlong.shop.core.blind.entity.dto.BlindBoxOrderDTO;
import com.xinlong.shop.core.blind.service.IBlindBoxOrderService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 盲盒订单表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-06-13
 */
@RestController
@Api(tags = "盲盒订单管理API")
@RequestMapping("/blind-box/blind-box-order")
public class BlindBoxOrderController {

    private final IBlindBoxOrderService blindBoxOrderService;

    public BlindBoxOrderController(IBlindBoxOrderService blindBoxOrderService) {
        this.blindBoxOrderService = blindBoxOrderService;
    }


    @ApiOperation(value = "获取盲盒订单分页列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页数", required = true, paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = true, paramType = "query"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = BlindBoxOrderDTO.class))
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json")
    public R page(Integer current, Integer size, BlindBoxOrderQueryParam queryParam) {
        IPage<BlindBoxOrderDTO> page = new Page<>(current, size);
        page = this.blindBoxOrderService.findByPage(page, queryParam);
        return R.success("操作成功", page);
    }

//    @RequestMapping(value = "", method = RequestMethod.POST)
//    public R save(@Valid @RequestBody BlindBoxOrder blindBoxOrder ){
//        this.blindBoxOrderService.save(blindBoxOrder);
//        return R.success("操作成功");
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public R update(@Valid @RequestBody BlindBoxOrder blindBoxOrder, @PathVariable("id") Integer id){
//        this.blindBoxOrderService.update(blindBoxOrder, id);
//        return R.success("操作成功",blindBoxOrder);
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public R delete(@PathVariable("id") Integer id) {
//        this.blindBoxOrderService.delete(id);
//        return R.success("删除成功");
//    }

    @RequestMapping(value = "/buy-success", method = RequestMethod.GET)
    public R testBuySuccess(){
        blindBoxOrderService.paySuccess("3230614160002", "no pay order sn");
        return R.success("test");
    }

}
