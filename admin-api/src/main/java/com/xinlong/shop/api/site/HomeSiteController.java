package com.xinlong.shop.api.site;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.site.entity.HomeSite;
import com.xinlong.shop.core.site.entity.dto.HomeBlindBoxDTO;
import com.xinlong.shop.core.site.entity.vo.HomeSiteBlindVO;
import com.xinlong.shop.core.site.service.IHomeSiteBlindService;
import com.xinlong.shop.core.site.service.IHomeSiteService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 首页位置 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@RestController
@Validated      // get请求验证参数时需要
@Api(tags = "首页管理")
@RequestMapping("/site/home-site")
public class HomeSiteController {

    private final IHomeSiteService homeSiteService;

    private final IHomeSiteBlindService homeSiteBlindService;


    public HomeSiteController(IHomeSiteService homeSiteService, IHomeSiteBlindService homeSiteBlindService) {
        this.homeSiteService = homeSiteService;
        this.homeSiteBlindService = homeSiteBlindService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<HomeSite> page = new Page<>(current, size);
        page = this.homeSiteService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody HomeSite homeSite ){
        homeSite.setCreateTime(DateUtil.currentSeconds());
        this.homeSiteService.save(homeSite);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody HomeSite homeSite, @PathVariable("id") Integer id){
        this.homeSiteService.update(homeSite, id);
        return R.success("操作成功",homeSite);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.homeSiteService.delete(id);
        return R.success("删除成功");
    }

    @ApiOperation(value = "首页位置保存选择盲盒")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功"))
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public R selectBlindBox(@RequestBody HomeSiteBlindVO homeSiteBlindVO){
        this.homeSiteBlindService.batchSaveBlindBox(homeSiteBlindVO);
        return R.success("操作成功");
    }

    @ApiOperation(value = "获得某个位置已选择的盲盒列表")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功"))
    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public R selectBlindBox(@NotNull(message = "广告位编码不能为空") String siteCode){
        List< HomeBlindBoxDTO> list = this.homeSiteBlindService.findBlindBoxList(siteCode);
        return R.success(list);
    }


//    @ApiOperation(value = "获取所有首页位置")
//    @RequestMapping(value = "/list", method = RequestMethod.GET)
//    public R list() {
//        List<HomeSite> list = this.homeSiteService.list();
//        return R.success(list);
//    }

}
