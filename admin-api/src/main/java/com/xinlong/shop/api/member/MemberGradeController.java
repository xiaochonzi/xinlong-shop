package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.member.entity.MemberGrade;
import com.xinlong.shop.core.member.service.IMemberGradeService;

import java.util.List;

/**
 * <p>
 * 会员等级 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@RestController
@RequestMapping("/member/member/grade")
public class MemberGradeController {

    private final IMemberGradeService memberGradeService;

    public MemberGradeController(IMemberGradeService memberGradeService) {
        this.memberGradeService = memberGradeService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<MemberGrade> page = new Page<>(current, size);
        page.addOrder(new OrderItem("sort", true));
        page = this.memberGradeService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@RequestBody MemberGrade memberGrade ){
        this.memberGradeService.save(memberGrade);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@RequestBody MemberGrade memberGrade, @PathVariable("id") Integer id){
        this.memberGradeService.update(memberGrade, id);
        return R.success("操作成功",memberGrade);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberGradeService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list() {
        List<MemberGrade> list = this.memberGradeService.list();
        return R.success("操作成功", list);
    }

}
