package com.xinlong.shop.api.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.order.OrderQueryParam;
import com.xinlong.shop.core.order.entity.Order;
import com.xinlong.shop.core.order.service.IOrderService;
import com.xinlong.shop.core.site.entity.Logistics;
import com.xinlong.shop.core.site.service.ILogisticsService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@Api(tags = "订单管理")
@RestController
@RequestMapping("/order/order")
public class OrderController {

    private final IOrderService orderService;
    private final ILogisticsService logisticsService;

    public OrderController(IOrderService orderService, ILogisticsService logisticsService) {
        this.orderService = orderService;
        this.logisticsService = logisticsService;
    }

    @ApiOperation(value = "获取订单列表分页")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Order.class))
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json")
    public R page(Integer current, Integer size, OrderQueryParam orderQueryParam){
        IPage<Order> page = new Page<>(current, size);
        page = this.orderService.selectPage(page, orderQueryParam);
        return R.success(page);
    }

    @ApiOperation(value = "获取物流公司列表")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Logistics.class))
    @RequestMapping(value = "/logistics", method = RequestMethod.GET, produces = "application/json")
    public R logistics(){
        List<Logistics> list = logisticsService.listAll();
        return R.success(list);
    }

    @ApiOperation(value = "订单发货")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功"))
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, paramType = "query"),
            @ApiImplicitParam(name = "logisticsId", value = "物流公司Id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "logisticsName", value = "物流公司名称", required = true, paramType = "query"),
            @ApiImplicitParam(name = "logisticsSn", value = "物流单号", required = true, paramType = "query"),
    })
    @RequestMapping(value = "/shipment", method = RequestMethod.PUT, produces = "application/json")
    public R shipment(Integer orderId, Integer logisticsId, String logisticsName, String logisticsSn){
        orderService.shipment(orderId, logisticsId, logisticsName, logisticsSn);
        return R.success();
    }



}
