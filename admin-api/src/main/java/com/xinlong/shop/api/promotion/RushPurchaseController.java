package com.xinlong.shop.api.promotion;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseDTO;
import com.xinlong.shop.core.promotion.mapstruct.RushPurchaseStruct;
import com.xinlong.shop.core.promotion.service.IRushPurchaseDetailService;
import com.xinlong.shop.framework.service.ISysPageStatisticsService;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.xinlong.shop.core.promotion.service.IRushPurchaseService;
import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * <p>
 * 抢购活动表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
@RestController
@RequestMapping("/promotion/rush-purchase")
public class RushPurchaseController {

    private final IRushPurchaseService rushPurchaseService;

    private final RushPurchaseStruct rushPurchaseStruct;

    private final ISysPageStatisticsService sysPageStatisticsService;

    private final IRushPurchaseDetailService rushPurchaseDetailService;

    public RushPurchaseController(IRushPurchaseService rushPurchaseService, RushPurchaseStruct rushPurchaseStruct, ISysPageStatisticsService sysPageStatisticsService, IRushPurchaseDetailService rushPurchaseDetailService) {
        this.rushPurchaseService = rushPurchaseService;
        this.rushPurchaseStruct = rushPurchaseStruct;
        this.sysPageStatisticsService = sysPageStatisticsService;
        this.rushPurchaseDetailService = rushPurchaseDetailService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<RushPurchase> page = new Page<>(current, size);
        page.addOrder(new OrderItem("start_time",false));
        page = this.rushPurchaseService.page(page);
        // 临时循环查询  后期改进sql里
        page.getRecords().forEach(rushPurchase -> {
            int num = this.sysPageStatisticsService.visitNum(rushPurchase.getId(), "rush_purchase");
            BigDecimal totalPrice = this.rushPurchaseDetailService.getTotleByRushPurchaseId(rushPurchase.getId());
            rushPurchase.setVisitNum(num);
            rushPurchase.setTotalPrice(totalPrice);

        });
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody RushPurchaseDTO rushPurchaseDTO ){
        if (rushPurchaseDTO.getActivityTime()[0] == null || rushPurchaseDTO.getActivityTime()[1] == null ) {
            return R.error("活动时间不能为空");
        }
        // 数据转换
        RushPurchase rushPurchase = rushPurchaseStruct.toRushPurchase(rushPurchaseDTO);
        rushPurchase.setStartTime(rushPurchaseDTO.getActivityTime()[0] / 1000);
        rushPurchase.setEndTime(rushPurchaseDTO.getActivityTime()[1] / 1000);
        rushPurchase.setCreateTime(DateUtil.currentSeconds());
        this.rushPurchaseService.save(rushPurchase);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody RushPurchaseDTO rushPurchaseDTO, @PathVariable("id") Integer id){
        // 数据转换
        RushPurchase rushPurchase = rushPurchaseStruct.toRushPurchase(rushPurchaseDTO);
        rushPurchase.setStartTime(rushPurchaseDTO.getActivityTime()[0] / 1000);
        rushPurchase.setEndTime(rushPurchaseDTO.getActivityTime()[1] / 1000);
        this.rushPurchaseService.update(rushPurchase, id);
        return R.success("操作成功",rushPurchase);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.rushPurchaseService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/status", method = RequestMethod.PUT)
    public R status(Integer id, Integer status) {
        if (status == null) {
            status = 0;
        }
        this.rushPurchaseService.updateStatus(id, status);
        //this.goodsService.updateStatus(id, status);
        return R.success("操作成功");
    }

//    @RequestMapping(value = "/visit-num", method = RequestMethod.GET)
//    public R visitNum(Integer id) {
//        int num = this.sysPageStatisticsService.visitNum(id, "rush_purchase");
//        return R.success("操作成功", num);
//    }

}
