package com.xinlong.shop.api.common;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysMenu;
import com.xinlong.shop.framework.core.model.SysMenuDTO;
import com.xinlong.shop.framework.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 菜单管理
 * @Date: Created in 17:54 2022/7/15
 */
@RestController
@RequestMapping("/dev/menu")
public class DevMenuController {

    private final ISysMenuService sysMenuService;

    public DevMenuController(ISysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(){
        List<SysMenu> menus = sysMenuService.findAllMenus();
        return R.success("操作成功",menus);
    }

    /**
     * 返回树结构的所有菜单列表
     * @return
     */
    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    public R tree() {

        List<SysMenuDTO> treeMenus = sysMenuService.findMenusTree();

        return R.success("操作成功",treeMenus);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public R save(@Valid @RequestBody SysMenu sysMenu ){
        sysMenuService.add(sysMenu);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public R update(@Valid @RequestBody SysMenu sysMenu ){
        sysMenuService.update(sysMenu);
        return R.success("操作成功",sysMenu);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        UpdateWrapper<SysMenu> sysMenuDeleteWrapper = new UpdateWrapper<>();
        sysMenuDeleteWrapper.eq("id",id);
        sysMenuService.remove(sysMenuDeleteWrapper);
        return R.success("删除成功");
    }

}
