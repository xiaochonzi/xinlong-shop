package com.xinlong.shop.api.member;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.member.entity.MemberActivityIncome;
import com.xinlong.shop.core.member.service.IMemberActivityIncomeService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 会员收益表 前端控制器
 * </p>
 * 2023 0330: 管理界面待对接
 * @author Sylow
 * @since 2023-03-30
 */
@RestController
@RequestMapping("/member/activity-income")
public class MemberActivityIncomeController {

    private final IMemberActivityIncomeService memberActivityIncomeService;

    public MemberActivityIncomeController(IMemberActivityIncomeService memberActivityIncomeService) {
        this.memberActivityIncomeService = memberActivityIncomeService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, Integer memberId){
        IPage<MemberActivityIncome> page = new Page<>(current, size);
        page = this.memberActivityIncomeService.page(page, memberId);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberActivityIncome memberActivityIncome, @PathVariable("id") Integer id){
        this.memberActivityIncomeService.update(memberActivityIncome, id);
        return R.success("操作成功", memberActivityIncome);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberActivityIncomeService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/income-summary", method = RequestMethod.GET)
    public R getIncomeSummary(Integer memberId) {
        Long startTime = DateUtil.beginOfMonth(new Date()).getTime() / 1000;
        Long endTime = DateUtil.endOfMonth(new Date()).getTime() / 1000;
        BigDecimal allIncome = this.memberActivityIncomeService.getTotal(memberId);
        BigDecimal monthIncome = this.memberActivityIncomeService.getTotalByTime(memberId, startTime, endTime);
        Map<String, BigDecimal> result = new HashMap<>();
        result.put("allIncome", allIncome);
        result.put("monthIncome", monthIncome);
        return R.success( result);
    }

}
