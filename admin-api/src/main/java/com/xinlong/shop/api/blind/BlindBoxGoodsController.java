package com.xinlong.shop.api.blind;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import com.xinlong.shop.core.blind.service.IBlindBoxGoodsService;
import javax.validation.Valid;

/**
 * <p>
 * 盲盒关联商品表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@RestController
@RequestMapping("/blind-box/blind-box-goods")
public class BlindBoxGoodsController {

    private final IBlindBoxGoodsService blindBoxGoodsService;

    public BlindBoxGoodsController(IBlindBoxGoodsService blindBoxGoodsService) {
        this.blindBoxGoodsService = blindBoxGoodsService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<BlindBoxGoods> page = new Page<>(current, size);
        page = this.blindBoxGoodsService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody BlindBoxGoods blindBoxGoods ){
        this.blindBoxGoodsService.save(blindBoxGoods);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody BlindBoxGoods blindBoxGoods, @PathVariable("id") Integer id){
        this.blindBoxGoodsService.update(blindBoxGoods, id);
        return R.success("操作成功",blindBoxGoods);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.blindBoxGoodsService.delete(id);
        return R.success("删除成功");
    }

}
